'''
Playing with RxPY
https://rxpy.readthedocs.io/en/latest/index.html
'''

import rx
from rx import operators as ops

def my_print(x):
    print(F"my_print: x={x}")
    return x

obsv0 = rx.of(*list(range(5)))
obsv1 = obsv0.pipe(ops.map(my_print))

obsv1.subscribe(on_next=lambda x: print(F"got 1-{x}"))
obsv1.subscribe(on_next=lambda x: print(F"got 2-{x}"))
