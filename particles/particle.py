import math as m
import numpy as np
import random
import pygame as pg

from pbgames.physics.particle import Particle2D
from pbgames.colors import colors
from pbgames.utils import distance, normalized
from pbgames.physics.gravity import gravity, G
# from pbgames.physics.gravity import FORCEMULT

class Particle(Particle2D):
    @classmethod
    def new(cls, config):
        ''' create a new Particle based on config values and Sun '''
        w_width = float(config.right) - float(config.left)
        w_width2 = w_width/2
        w_height = float(config.top) - float(config.bottom)
        w_height2 = w_height/2
        x = (random.random() * w_width) - w_width2
        y = (random.random() * w_height) - w_height2
        pos = x, y
        pt = cls(*pos, mass=1.0)

        r = distance(pos, Sun.pos)
        vmag = m.sqrt(G * Sun.mass / r)  # * FORCEMULT / 10

        fps = float(config.fps)
        zzz = 1.0/fps
        speedup = float(config.speedup)
        dt = zzz * speedup

        Fg = gravity(pt, Sun)
        pt.vel = normalized(np.array([-Fg[1], Fg[0]])) * vmag
        pt.vel = pt.vel * dt
        return pt

    def move(self, game):
        self.fg = Fg = gravity(self, Sun)
        self.accel(Fg*game.dt)
        super(Particle, self).move()
        # super(Particle, self).move(C=game.dt)
        # self.pos = self.pos + self.vel * game.dt

    def draw(self, game):
        screen = game.screen

        # draw position
        p0 = game.w2s(*self.pos)
        pg.draw.circle(screen, colors['white'], p0, 5)

        # draw force
        p1 = self.pos+self.fg*10
        p1 = game.w2s(*p1)
        pg.draw.line(screen, colors['red1'], p0, p1, width=3)

        # draw velocity
        p2 = self.pos+self.vel
        p2 = game.w2s(*p2)
        pg.draw.line(screen, colors['yellow1'], p0, p2, width=3)

Sun = Particle(0.0, 0.0, mass=10_000_000)
