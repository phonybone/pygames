"""
2D Particle simulator
"""
import random
import pygame as pg
from pbgames.gamebase import Game
from pbgames.colors import colors
from pbutils.argparsers import parser_stub, wrap_main
from particle import Particle, Sun
from particle_boid import ParticleBoid

random.seed(0)


class ParticleGame(Game):
    def __init__(self, config):
        super(ParticleGame, self).__init__(config.X, config.Y,
                                           config.bottom, config.left,
                                           config.top, config.right,
                                           config.background_color,
                                           config.fps, config.speedup
                                           )
        self.movables = []
        self.drawables = []
        self.load_particles(config)

    def load_particles(self, config):
        for _ in range(int(config.n_particles)):
            pt = ParticleBoid.new(config)
            # pt = Particle.new(config)
            self.movables.append(pt)
            self.drawables.append(pt)

    def render_frame(self):
        # self.screen.fill(self.background)
        for obj in self.movables:
            obj.move(self)
        for obj in self.drawables:
            obj.draw(self)

        # draw sun:
        pos = self.w2s(*Sun.pos)
        pg.draw.circle(self.screen, colors['yellow2'], pos, 20)


def main(config):
    game = ParticleGame(config)
    game.start()
    game.loop()

    return 0

def make_parser():
    parser = parser_stub(__doc__)
    parser.add_argument('-X', '--width', default=640, type=int, help='width of world')
    parser.add_argument('-Y', '--height', default=320, type=int, help='height of world')
    parser.add_argument('--n-boids', default=10, type=int, help='number of boids')
    parser.add_argument('arg', nargs='*')

    return parser

if __name__ == '__main__':
    parser = make_parser()
    wrap_main(main, parser)    
