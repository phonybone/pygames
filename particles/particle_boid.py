import numpy as np
import pygame as pg
from pbgames.transform import rot2d, translate2d, scale2d
from pbgames.utils import to_radians
from pbgames.colors import colors
from particle import Particle

_X = 0
_Y = 1

class ParticleBoid(Particle):
    '''
    This is just particles, but drawn in the shape of boids.
    '''
    # for drawing:
    dpoints2 = (
        np.array((-15, 15)),
        np.array((-15, -15)),
        np.array((30, 0))
    )
    dpoints3 = (
        np.array((-15, 15, 1)),
        np.array((-15, -15, 1)),
        np.array((30, 0, 1))
    )
    scale_f = 1e-1

    @property
    def heading(self):
        ''' return heading in radians '''
        return to_radians(self.vel[_X], self.vel[_Y])

    def draw(self, game):
        # print(F"{self}: heading={m.degrees(self.heading):.0f} ({self.heading:.2f})")
        scl = scale2d(ParticleBoid.scale_f, ParticleBoid.scale_f)
        rot = rot2d(-self.heading)
        trnslt = translate2d(self.pos[_X], self.pos[_Y])
        rottrn = np.matmul(scl, np.matmul(rot, trnslt))
        p0s = [pt.dot(rottrn)[:2] for pt in self.dpoints3]
        # print(F"p0s: {p0s}")
        pts = [game.w2s(*pt) for pt in p0s]
        pts.append(pts[0])      # make a loop
        # print(F"pts: {pts}")
        lastpt = pts[0]
        for pt in pts[1:]:
            start = (lastpt[_X], lastpt[_Y])
            end = (pt[_X], pt[_Y])
            # print(F"boid: pg.draw.line(screen, color, {start}, {end}, width=3)")
            pg.draw.line(game.screen, colors['red1'], start, end, width=3)
            lastpt = pt

        # draw velocity:
        # print(F"pos: {self.pos} vel: {self.vel}")
        p0 = game.w2s(*self.pos)
        p2 = self.pos+self.vel
        p2 = game.w2s(*p2)
        pg.draw.line(game.screen, colors['yellow1'], p0, p2, width=3)
        # print(F"vel: pg.draw.line(screen, color, {p0}, {p2}, width=3)")

