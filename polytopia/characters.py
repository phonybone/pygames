from functools import partial
from character import Character

Warrior = partial(Character, kind='Warrior', health=10, offense=2, defense=2, att_range=1, movement=1)
Rider = partial(Character, kind='Rider', health=10, offense=2, defense=1, att_range=1, movement=3)
Archer = partial(Character, kind='Archer', health=10, offense=2, defense=1, att_range=2, movement=1)
Defender = partial(Character, kind='Defender', health=15, offense=1, defense=3, att_range=1, movement=1)
