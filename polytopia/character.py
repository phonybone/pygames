'''
Polytopia character
'''

from postion import Postion

class Character:
    def __init__(self,
                 kind,
                 health,
                 offense,
                 defense,
                 att_range,
                 movement,
                 position):
        self.kind = kind
        self.health = health
        self.offense = offense
        self.defense = defense
        self.att_range = att_range
        self.movement = movement,
        self.position = position

    def moves(self):
        ''' return a list of permitted moves for the character '''
        pass
    
