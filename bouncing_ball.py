import sys
import threading as th
import time
import pygame
pygame.init()

size = width, height = 320, 240
speed = [2, 2]
black = 0, 0, 0

screen = pygame.display.set_mode(size)

ball = pygame.image.load("ball.gif")
ballrect = ball.get_rect()

start_drawing = th.Event()
flip_ready = th.Event()
game_over = th.Event()
H = 0
# zzz = 1.0/30.0
zzz = 2

def event_report():
    SD = F"{start_drawing.is_set()}"
    FR = F"{flip_ready.is_set()}"
    return F"SD={SD} FR={FR}"

def flip():
    while True:
        print(F"waiting to flip_ready ({event_report()})")
        flip_ready.wait()
        print(f'about to flip frame: {event_report()}')
        pygame.display.flip()
        screen.fill(black)
        flip_ready.clear()

flipper = th.Thread(target=flip, daemon=True)
flipper.start()

def sleep(zzz):
    global H
    game_over.clear()
    flip_ready.clear()
    start_drawing.set()
    while True:
        start_drawing.set()
        flip_ready.clear()
        print(F"sleeper: {event_report()}")
        time.sleep(zzz)
        H += zzz
sleeper = th.Thread(target=sleep, args=(zzz,), daemon=True)
sleeper.start()

def draw(H)
while True:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            sys.exit()

    print(f'waiting to start_drawing: {event_report()}')
    start_drawing.wait()

    ballrect = ballrect.move(speed)
    if ballrect.left < 0 or ballrect.right > width:
        speed[0] = -speed[0]
    if ballrect.top < 0 or ballrect.bottom > height:
        speed[1] = -speed[1]
    screen.blit(ball, ballrect)

    start_drawing.clear()
    flip_ready.set()

