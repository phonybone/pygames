'''
Drawable/movable graph objects for pygames
'''
import pygame as pg
from test_graph import graph
import colors

class GraphObj:
    def __init__(self, g):
        self.g = g

    # def move(self, t):
    #     for nde in self.g.nodes:
    #         nde['x'] += nde['dx']
    #         nds['y'] += nde['dy']

    def draw(self, screen):
        # draw edges
        for edg, eattrs in self.g.edges.items():
            idx0, idx1 = edg
            n0 = self.g.nodes[idx0]
            n1 = self.g.nodes[idx1]
            color = getattr(colors, eattrs['color'])
            width = eattrs['width']
            pg.draw.line(screen, color, (n0['x'], n0['y']), (n1['x'], n1['y']), width)

        # draw nodes
        for node, nattrs in self.g.nodes.items():
            shaper = getattr(pg.draw, nattrs['shape'])
            color = getattr(colors, nattrs['color'])
            args = nattrs['args']
            shaper(screen, color, (nattrs['x'], nattrs['y']), *args)

graph_obj = GraphObj(graph)
