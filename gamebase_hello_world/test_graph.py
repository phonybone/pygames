'''
'''
import networkx as nx

node_data = (
    (1, 100, 100, 'circle', (15,), 'blue'),
    (2, 800, 200, 'circle', (15,), 'blue'),
    (3, 300, 800, 'circle', (15,), 'blue'),
    (4, 500, 500, 'circle', (15,), 'blue'),
)

edge_data = (
    (4, 1, 'yellow', 5),
    (4, 2, 'yellow', 10),
    (4, 3, 'yellow', 8),
    (2, 1, 'red', 12),
)

graph = g = nx.Graph()
for nd in node_data:
    idx, x, y, shape, args, color = nd
    g.add_node(idx, x=x, y=y, shape=shape, args=args, color=color)

for ed in edge_data:
    i0, i1, color, width = ed
    g.add_edge(i0, i1, color=color, width=width)

for n in g.nodes:
    print(F"n[{n}]={g.nodes[n]}")

for e in g.edges:
    print(F"e[{e}]={g.edges[e]}")
