import pygame as pg

class Ball:
    def __init__(self, game, x, y, dx, dy):
        self.game = game
        self.img = pg.image.load("ball.gif")
        self.rect = self.img.get_rect()
        self.rect.x = x
        self.rect.y = y
        self.speed = [dx, dy]

    def move(self, t):
        ball = self.rect
        if ball.left < 0 or ball.right > self.game.width:
            self.speed[0] = -self.speed[0]
        if ball.top < 0 or ball.bottom > self.game.height:
            self.speed[1] = -self.speed[1]
        self.rect = ball.move(self.speed)

    def draw(self, screen):
        screen.blit(self.img, self.rect)
        
