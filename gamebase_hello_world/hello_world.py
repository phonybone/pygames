'''
Do the bouncing ball game
'''
from bbgame import BBGame
import colors

def main():
    game = BBGame(1000, 800, colors.black)
    game.start()
    game.loop()

if __name__ == '__main__':
    main()
