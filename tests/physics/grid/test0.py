import sys, os
import numpy as np
import unittest

from pygames.physics.grid import new_grid, grid_size
from pygames.physics.utils2d import almost_equal
from pygames.transform import world2grid

class TestSomething(unittest.TestCase):
    
    def setUp(self):
        print
        
    def test_something(self):

        # create a grid:
        gW = 10.0
        gH = 12.0
        grid = new_grid(gW, gH, lambda i,j: i+j) # any old lambda for now
        size = grid_size(grid)
        # print 'size: {}'.format(str(size))

        # create a world plane:
        wW = 100                    # world width
        wH = 125                    # world height
        wX = 75                      # world X offset
        wY = 50                      # world Y offset
        t = world2grid(size[0], size[1], wX, wX+wW, wY, wY+wH)
        # print 't:\n{}'.format(t)
        # print

        # check all four corners of world plane match grid corners:
        w_origin = np.array((wX, wY, 1), dtype=float)
        g_origin = w_origin.dot(t)
        # print '{} -> {}'.format(str(w_origin), str(w_origin.dot(t)))
        self.assertTrue(almost_equal(g_origin[0], 0), '{} != 0'.format(g_origin[0]))
        self.assertTrue(almost_equal(g_origin[1], gH), '{} != gH'.format(g_origin[0]))

        w_ul = np.array((wX, wY+wH, 1), dtype=float)
        g_ul = w_ul.dot(t)
        # print '{} -> {}'.format(str(w_ul), str(g_ul))
        self.assertTrue(almost_equal(g_ul[0], 0), '{} != 0'.format(g_ul[0]))
        self.assertTrue(almost_equal(g_ul[1], 0), '{} != 0'.format(g_ul[1]))

        w_ur = np.array((wW+wX, wH+wY, 1), dtype=float)
        g_ur = w_ur.dot(t)
        # print '{} -> {}'.format(str(w_ur), str(g_ur))
        self.assertTrue(almost_equal(g_ur[0], gW), '{} != {}'.format(g_ur[0], gW))
        self.assertTrue(almost_equal(g_ur[1], 0), '{} != {}'.format(g_ur[1], 0))

        w_br = np.array((wX+wW, wY, 1), dtype=float)
        g_br = w_br.dot(t)
        # print '{} -> {}'.format(str(w_br), str(g_br))
        self.assertTrue(almost_equal(g_br[0], gW), '{} != {}'.format(g_ur[0], gW))
        self.assertTrue(almost_equal(g_br[1], gH), '{} != {}'.format(g_ur[1], gH))
