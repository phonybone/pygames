'''
'''
import unittest
from pygames.transform import grid2unit, apply

class TestGrid2unit(unittest.TestCase):
    
    def setUp(self):
        print
        
    def test_grid2unit(self):
        gWidth, gHeight = 10, 10
        g2u = grid2unit(gWidth, gHeight)

        self.assertEqual(apply(g2u, 0, 0), (0, 1))
        self.assertEqual(apply(g2u, 0, gHeight), (0, 0))
        self.assertEqual(apply(g2u, gWidth, 0), (1, 1))
        self.assertEqual(apply(g2u, gWidth, gHeight), (1, 0))

        self.assertEqual(apply(g2u, gWidth/2, gHeight/2), (0.5, 0.5))
