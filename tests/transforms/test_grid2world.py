'''
'''
import unittest
import random
from pygames.transform import grid2world, tr

class TestGrid2unit(unittest.TestCase):
    
    def _test_g2u(self, gW, gH, x0, x1, y0, y1):
        cx = (x1-x0)/2 + x0
        cy = (y1-y0)/2 + y0

        g2w = grid2world(gW, gH, x0, x1, y0, y1)

        self.assertEqual(tr(g2w, 0, 0), (x0, y1))
        self.assertEqual(tr(g2w, 0, gH), (x0, y0))
        self.assertEqual(tr(g2w, gW, 0), (x1, y1))
        self.assertEqual(tr(g2w, gW, gH), (x1, y0))
        self.assertEqual(tr(g2w, gW/2, gH/2), (cx, cy))
        
    def test_grid2unit(self):
        gWidth, gHeight = 640, 480
        x0, x1, y0, y1 = 0, 100, 0, 100 # world coords rect
        self._test_g2u(gWidth, gHeight, x0, x1, y0, y1)

    def test_grid2unit_random_world(self):
        gWidth, gHeight = 640, 480
        x0 = random.random()
        x1 = random.random()
        y0 = random.random()
        y1 = random.random()
        self._test_g2u(gWidth, gHeight, x0, x1, y0, y1)
