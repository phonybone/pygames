import sys

def _next_id():
    _id=0
    while True:
        yield _id
        _id+=1


# generators within classes:

class uuidObject(object):
    __next_id=_next_id()
    
    def __init__(self):
        self.__uuid=next(uuidObject.__next_id)

    @property
    def uuid(self):
        return self.__uuid


class Device(uuidObject):
    def __str__(self):
        return str(self.uuid)

for _ in range(3):
    d=Device()
    print 'd',d

next_id=_next_id()
print next(next_id)
print next(next_id)
print next(next_id)


def gen_range(x):
    for i in xrange(x):
        yield i

def gen2_range():
    '''
    create and run a nested loop generator
    '''
    def gen(x,y):
        for i in xrange(x):
            for j in xrange(y):
                yield (i,j)

    for k,t in enumerate(gen(3,4)):
        print '%d: i=%d, j=%d' % (k,t[0], t[1])


def generator_expression(s):
    return (x for x in xrange(s))

import re
def build_match_and_apply_functions(pattern, search, replace):
    def matches_rule(word):
        return re.search(pattern, word)
    def apply_rule(word):
        return re.sub(search, replace, word)
    return (matches_rule, apply_rule)



gen=generator_expression(4)
l=list(gen)
print 'l is %s' % l


pattern=r'a'
search=r'a'
replace='b'
match, appl=build_match_and_apply_functions(pattern, search, replace)
print 'match(fart)=%s' % match('fart')
print 'appl(fart)=%s' % appl('fart')
print

#gen_range(5)
#gen2_range()

def closed_gen(x):
    '''
    create and return a generator based on a closure
    '''
    def c(x):
        while x>0:
            x-=1
            yield x
    return c(x)


def line(p0, p1, n):
    ''' return a generator that yields n points on a line '''
    assert type(n) is int and n > 0 
    dx = p1[0] - p0[0]
    dy = p1[1] - p0[1]
    h = 1.0/n

    def l(p0, p1, n):
        i = 0
        while i <= n:
            t = i * h;
            yield (p0[0] + t*dx, p0[1] + t*dy)
            i += 1
    return l(p0, p1, n)

import math
def _to_rads(degrees):
    return degrees / 180 * 3.14159254

def circle(c, r, a1=0, a2=360, n=24):
    ''' return a generator that yields points on a circle '''
    assert type(n) is int and n > 0
    dTheta = (a2-a1)/float(n)

    def _c(c, r, n):
        i = 0
        while i <= n:
            theta = _to_rads(i * dTheta)
            x = c[0] + r*math.cos(theta)
            y = c[1] + r*math.sin(theta)
            yield (x,y)
            i += 1
    return _c(c, r, n)

p0 = (32.38, -28.98)
p1 = (-10, -10)
l = line(p0, p1, 10)
for p in l:
    print 'p: {:.2f}, {:.2f}'.format(p[0], p[1])

c = (0, 0)                   
r = 1
circ = circle(c, r, 0, 180, 24)
for p in circ:
    print 'p: {:.4f}, {:.4f}'.format(p[0], p[1])


C=closed_gen(5)
print 'C is %s' % C
for i in C:
    print i
