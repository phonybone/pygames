'''
(English) Letters represented as graphs.
'''

import networkx as nx
from graphics import Surface

class Letter(object):
    def __init__(self, letter):
        self.letter = letter
        self.g = nx.Graph()

    def draw(self, surface):
        pass

class LetterA(Letter):
    def __init__(self):
        Letter.__init__(self, 'A')
        self.g.add_node(1, p=(0.5,  1.0)) #     1
        self.g.add_node(2, p=(0.25, 0.5)) #    / \
        self.g.add_node(3, p=(0.0,  0.0)) #   2---4
        self.g.add_node(4, p=(0.75, 0.5)) #  /     \
        self.g.add_node(5, p=(1.0,  0.0)) # 3       5

        self.g.add_edge(3,2)
        self.g.add_edge(2,1)
        self.g.add_edge(2,4)
        self.g.add_edge(1,4)
        self.g.add_edge(4,5)

class LetterI(Letter):
    def __init__(self):
        Letter.__init__(self, 'I')
        self.g.add_node(1, p=(0.5, 1.0))
        self.g.add_node(2, p=(0.5, 0.0))
        self.g.add_edge(1,2)

    def toL(self):
        pass

    def toT(self):
        n3 = self.g.nodes[1]
        n3.p = (0.0, 1.0)
        

class LetterL(Letter):
    def __init__(self):
        Letter.__init__(self, 'L')
        self.g.add_node(1, p=(0.0, 1.0))
        self.g.add_node(2, p=(0.0, 0.0))
        self.g.add_node(3, p=(1.0, 0.0))


class LetterT(Letter):
    def __init__(self):
        Letter.__init__(self, 'T')
        self.g.add_node(1, p=(0.5, 1.0))
        self.g.add_node(2, p=(.05, 0.0))
        self.g.add_node(3, p=(0.0, 1.0))
        self.g.add_node(4, p=(1.0, 0.0))

        self.g.add_edge(1,2)
        self.g.add_edge(1,3)
        self.g.add_edge(1,4)

if __name__ == '__main__':
    i = Letter('i')
    t = i.toT()
    for node in t.g.nodes():
        print(F"t-node: {node}")
