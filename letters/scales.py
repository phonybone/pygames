'''
Generators that yield transitions along a path.
'''

def line(p0, p1, n):
    ''' return a generator that yields n points on a line '''
    assert type(n) is int and n > 0 
    dx = p1[0] - p0[0]
    dy = p1[1] - p0[1]
    h = 1.0/n

    def l(p0, p1, n):
        i = 0
        while i <= n:
            t = i * h;
            yield (p0[0] + t*dx, p0[1] + t*dy)
            i += 1
    return l(p0, p1, n)

import math
def _to_rads(degrees):
    return degrees / 180 * 3.14159254

def circle(c, r, a1=0, a2=360, n=24):
    ''' return a generator that yields n points on an arc of a circle '''
    assert type(n) is int and n > 0
    dTheta = (a2-a1)/float(n)

    def _c(c, r, n):
        i = 0
        while i <= n:
            theta = _to_rads(i * dTheta)
            x = c[0] + r*math.cos(theta)
            y = c[1] + r*math.sin(theta)
            yield (x,y)
            i += 1
    return _c(c, r, n)
