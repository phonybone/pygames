import math

from numerics import path_length


class Lane:
    def __init__(self, id, speed_fn, path_fn):
        """
        Lane()
        speed_fn and pat_fn are functions that take a single parameter 't' where 0 <= t <= 1.
        speed_fn(t) returns the desired speed of the lane.
        path_fn(t) returns (x,y) coords of the lane position at t.
        """
        self.id = id
        self.speed_fn = speed_fn
        self.path_fn = path_fn
        self.cars = []
        self.exits = []         # list of other lane objects to which vehicles may exit
        self.__length = None
        
    @property
    def length(self):
        if self.__length is None:
            self.__length = path_length(self.path_fn)
            print(f'lane length={self.__length}')
        return self.__length


if __name__ == '__main__':
    def main():
        lane = Lane('cir0', speed, circle)
        print(f"lane length: {lane.length}")

    def circle(t):
        x = math.cos(t * math.pi)
        y = math.sin(t * math.pi)
        return x, y

    def speed(t):
        return 1

    main()
