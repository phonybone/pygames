import math
from particle import Particle2D


class Car(Particle2D):
    max_accel = 5
    max_decel = -10

    def __init__(self, x, y, dx, dy, mass):
        Particle2D.__init__(self, x, y, dx, dy, mass)
        self.t = 0              # distance down lane

    def adjust_speed(self, lane, h):
        """
        lane_speed: a function on [0, 1] that returns the max speed allowable for that position
        h: time step
        """
        dx = h * self.speed
        dt = dx / lane.length
        t_new = car.t + dt
        if t_new > 1.0:
            raise NextLaneEvent()
        print(f'dx={dx:.2f}, dt={dt:.2f}, t_new={t_new:.2f}')
        if t_new > 1.0:
            raise NextLaneEvent
        self.t = t_new

        # look ahead to lane_speed(t+h) to find target speed
        # import pdb; pdb.set_trace()

        V_lane = lane.speed_fn(t_new)
        A_tgt = V_lane - self.speed
        A_tgt = min(A_tgt, self.max_accel)
        A_tgt = max(A_tgt, self.max_decel)
        print(f'car.speed: {self.speed:.2f}, V_lane: {V_lane}, A_tgt: {A_tgt:.2f}')

        # apply accel/decel

        # if self.speed < 0.0001:
        #     self.dx += A_tgt
        #     self.dy = 0
        #     print(f'from stop: dx={A_tgt:.2f}, dy=0')
        # else:
        #     ratio = V_lane / self.speed
        #     print(f'moving: ratio={V_lane:.2f}/{self.speed:.2f}={ratio:.2f}')
        #     self.dx *= ratio
        #     self.dy *= ratio
        #     print(f'moving: dx={self.dx:.2f}, dy={self.dy:.2f}')
        import pdb; pdb.set_trace()



class NextLaneEvent(RuntimeError):
    pass


if __name__ == '__main__':
    from lane import Lane

    car = Car(x=0, y=0, dx=0, dy=0, mass=1000)

    def lane_speed_60(t):
        return 60
    
    def lane_speed_0_60(t):
        ''' rise 0 -> 60 '''
        return 60 * (1 - 1 / (1 + (t * t)))

    def lane_speed_0_60_0(t):
        return 30 * (1 - math.cos(2.0 * math.pi * t))

    def circle100(t):
        r = 100
        x = r * math.cos(t * math.pi)
        y = r * math.sin(t * math.pi)
        return x, y

    lane = Lane('cir0', lane_speed_60, circle100)
    h = 0.05
    while car.t < 1.0:
        print(f"\nt={car.t:.2f}")
        car.adjust_speed(lane, h)
        car.move()
        print(f"car: {car}")
