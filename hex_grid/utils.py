'''
Model a sphere using subdivided hexagons.
'''
import math
import numpy as np


def n3s(g):
    '''
    Iterator: yield all triples (n0, n1, n2) such that nodes n0, n1, and n2
    are all mutually connected (forming a "triangle").

    g: networkx.Graph
    '''
    cache = set()
    for n0, n1 in g.edges:
        nn0 = set(g.neighbors(n0))
        nn1 = set(g.neighbors(n1))
        for n2 in nn0.intersection(nn1):
            tri = tuple(sorted([n0, n1, n2]))
            if tri not in cache:
                yield tri
            cache.add(tri)


def common_nodes(g, *ns):
    '''
    for a graph and a list of nodes, find any nodes that are neighbors
    to all of the given nodes.  Return as a set().
    '''
    common = set(g.neighbors(ns[0]))
    for n in ns:
        common = common.intersection(set(g.neighbors(n)))
    return common


def bounding_box(g):
    ''' find the min and max x,y coords of a graph assuming our pos convention '''
    minx, miny = g.nodes[1]['pos']
    maxx, maxy = g.nodes[1]['pos']

    for node in g.nodes:
        if 'pos' not in g.nodes[node]:
            continue
        x, y = g.nodes[node]['pos']
        minx = min(minx, x)
        maxx = max(maxx, x)
        miny = min(miny, y)
        maxy = max(maxy, y)
    return minx, maxx, miny, maxy


def add_next_node(g):
    ''' Add a node whose label is the (size of g) + 1 '''
    nn = g.number_of_nodes() + 1
    g.add_node(nn)
    return nn


def divide_line(p0, p1, n):
    '''
    Divide a line into n equal parts.
    Return a tuple of N+1 (x,y) tuples (includes both endpoints)
    '''
    a0 = np.array(p0)
    a1 = np.array(p1)
    v = a1 - a0                 # translate to origin
    dv = v/float(n)
    return tuple([a0 + i*dv for i in range(n+1)])
    

def sphr2cart(r, lat, lon):
    x = r * math.cos(lat) * math.cos(lon)
    y = r * math.cos(lat) * math.sin(lon)
    z = r * math.sin(lat)
    return x, y, z


def unit_hex(radius=1.0):
    ''' Return a 6-tuple of 3d points defining a hexagon oriented
    in the x-y plane.
    '''
    return tuple([(radius * math.cos(i * math.pi / 3.0), radius * math.sin(i * math.pi / 3.0), 0, 1) for i in range(6)])


def hex2xy(r, c, s=1):
    ''' 
    given r, c, and a "side length", return the x,y coords of the center of the
    corresponding hex.
    Origin is at 0, 0 (upper left)
    r -> y, c -> x
    '''
    if r & 1:                   # odd row
        return (c+0.5)*s, r*s
    else:
        return c*s, r*s


def xy2hex(x, y, s=1):
    ''' 
    inverse of hex2xy
    return r, c for given x, y (and s)
    x -> c, y -> r
    '''
    r = int(y + 0.5)
    if r & 1:                   # odd row
        c = int(x)
    else:
        c = int(x+0.5)


int((x+0.5)/s), int((y+0.5

if __name__ == '__main__':
    for p in unit_hex():
        print(p)
