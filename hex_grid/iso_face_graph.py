'''
IsoFaceGraph class.
'''
import math
import networkx as nx
from itertools import combinations
import numpy as np
from collections import OrderedDict

from iso_vertex_graph import IsoVertexGraph
from utils import n3s, add_next_node, sphr2cart, divide_line, common_nodes


class IsoFaceGraph(nx.Graph):
    '''
    Graph of connected faces on the iso-soccer ball.
    '''
    def __init__(self, *args, **kwargs):
        nx.Graph.__init__(self, *args, **kwargs)

    @classmethod
    def from_iso_vertex_graph(cls):
        '''
        Given the iso vertex graph, construct the iso face graph.
        Resulting graph has 32 nodes.  Each node has either 5 or 6 edges, and is
        center of a face of the soccer-ball tiling.
        '''
        f = cls()
        iso = IsoVertexGraph()
        f.add_nodes_from(iso)

        edges = {
            1: (13, 14, 15, 16, 17),
            2: (13, 17, 18, 22, 23),
            3: (13, 14, 18, 19, 24),
            4: (14, 15, 19, 20, 25),
            5: (15, 16, 20, 21, 26),
            6: (16, 17, 21, 22, 27),
            7: (22, 23, 27, 28, 32),
            8: (18, 23, 24, 28, 29),
            9: (19, 24, 25, 29, 30),
            10: (20, 25, 26, 30, 31),
            11: (21, 26, 27, 31, 32),
            12: (28, 29, 30, 31, 32),
            13: (1, 2, 3, 14, 17, 18),
            14: (1, 3, 4, 13, 15, 19),
            15: (1, 4, 5, 14, 16, 20),
            16: (1, 5, 6, 15, 17, 21),
            17: (1, 2, 6, 13, 16, 22),
            18: (2, 3, 8, 13, 23, 24),
            19: (3, 4, 9, 14, 24, 25),
            20: (4, 5, 10, 15, 25, 26),
            21: (5, 6, 11, 16, 26, 27),
            22: (2, 6, 7, 17, 23, 27),
            23: (2, 7, 8, 18, 22, 28),
            24: (3, 8, 9, 18, 19, 29),
            25: (4, 9, 10, 19, 20, 30),
            26: (5, 10, 11, 20, 21, 31),
            27: (6, 7, 11, 21, 22, 32),
            28: (7, 8, 12, 23, 29, 32),
            29: (8, 9, 12, 24, 28, 30),
            30: (9, 10, 12, 25, 29, 31),
            31: (10, 11, 12, 26, 30, 32),
            32: (7, 11, 12, 27, 28, 31)
        }

        for node, es in edges.items():
            for e in es:
                f.add_edge(node, e)
        return f

    def is_pent_node(self, n):
        ''' is the node a pentagon node? '''
        return n <= 12

    @property
    def pent_nodes(self):
        return tuple(range(1,13))

    @property
    def hex_nodes(self):
        n_nodes = len(self)
        return tuple(range(13, n_nodes))

    def is_hex_node(self, n):
        ''' is the node a hex node? '''
        return n > 12

    def is_pole_node(self, n):
        ''' is the node one of the two poles? '''
        return n == 1 or n == 12

    def is_north_pent_node(self, n):
        return n <= 6

    def is_south_pent_node(self, n):
        return n > 6 and n <= 12

    def set_flat_positions(self, r):
        '''
        Set all node coords for the "flat" graph.
        '''
        # manually set vertex positions:
        root3r2 = math.sqrt(3) * 3 * r / 2
        self.nodes[1]['pos'] = (15/2*r, -root3r2)
        self.nodes[2]['pos'] = (0*r, 0)
        self.nodes[3]['pos'] = (3*r, 0)
        self.nodes[4]['pos'] = (6*r, 0)
        self.nodes[5]['pos'] = (9*r, 0)
        self.nodes[6]['pos'] = (12*r, 0)
        self.nodes[7]['pos'] = (-3/2*r, root3r2)
        self.nodes[8]['pos'] = (3/2*r, root3r2)
        self.nodes[9]['pos'] = (9/2*r, root3r2)
        self.nodes[10]['pos'] = (15/2*r, root3r2)
        self.nodes[11]['pos'] = (21/2*r, root3r2)
        self.nodes[12]['pos'] = (6*r, 2 * root3r2)

        # sectors determine which portions of a pent-face to draw
        self.nodes[1]['sectors'] = (1,2,3,4,6)
        self.nodes[2]['sectors'] = (1,2,3,4,6)
        self.nodes[3]['sectors'] = (1,2,3,4,6)
        self.nodes[4]['sectors'] = (1,2,3,4,6)
        self.nodes[5]['sectors'] = (1,2,3,4,6)
        self.nodes[6]['sectors'] = (1,2,3,4,6)
        self.nodes[7]['sectors'] = (1,3,4,5,6)
        self.nodes[8]['sectors'] = (1,3,4,5,6)
        self.nodes[9]['sectors'] = (1,3,4,5,6)
        self.nodes[10]['sectors'] = (1,3,4,5,6)
        self.nodes[11]['sectors'] = (1,3,4,5,6)
        self.nodes[12]['sectors'] = (1,3,4,5,6)

        # hex nodes
        X, Y = 0, 1
        x_offset = 1.5 * r
        y_offset = math.sqrt(3.0) * r / 2
        cy = self.nodes[2]['pos'][Y] - y_offset
        for node in range(13, 18):
            cx = self.nodes[node-11]['pos'][X] + x_offset
            self.nodes[node]['pos'] = (cx, cy)

        cy = self.nodes[2]['pos'][Y] + y_offset
        for node in range(18, 23):
            cx = self.nodes[node-5]['pos'][X]
            self.nodes[node]['pos'] = (cx, cy)

        cy = self.nodes[7]['pos'][Y] - y_offset
        for node in range(23, 28):
            cx = self.nodes[node-21]['pos'][X]
            self.nodes[node]['pos'] = (cx, cy)

        cy = self.nodes[7]['pos'][Y] + y_offset
        for node in range(28, 33):
            cx = self.nodes[node-21]['pos'][X] + x_offset
            self.nodes[node]['pos'] = (cx, cy)

    def subdivide(self):
        ''' build a new graph off self with the next level of subdivision '''
        g = type(self)()

        # copy original nodes and their positions:
        g.add_nodes_from(self)
        # for n in self.nodes:
        #     g.nodes[n]['pos'] = self.nodes[n]['pos']

        node_cache = {}
        tris = list(n3s(self))
        
        for i, tri in enumerate(tris):
            n0, n1, n2 = tri
            center = add_next_node(g)

            hex_nodes = []
            for m0, m1 in combinations(tri, 2):
                hex_nodes.extend(g._gather_m1m2(m0, m1, node_cache, hex_nodes))

            # radial edges:
            for n in hex_nodes:
                g.add_edge(center, n)

            # corner edges
            c0 = node_cache[n0][n1]
            c1 = node_cache[n0][n2]
            g.add_edge(c0, c1)
            
            c0 = node_cache[n1][n0]
            c1 = node_cache[n1][n2]
            g.add_edge(c0, c1)
                
            c0 = node_cache[n2][n0]
            c1 = node_cache[n2][n1]
            g.add_edge(c0, c1)
                
        return g

    def _gather_m1m2(self, n0, n1, node_cache, hex_nodes):
        '''
        add to hex_nodes from the "spoke" edge defined by n0, n1
        also record created nodes in node_cache.
        return two spoke nodes (created as needed)
        '''
        if n0 in node_cache and n1 in node_cache[n0]:
            # we've visited edge n0-n1, nodes already been added
            m0 = node_cache[n0][n1]
            m1 = node_cache[n1][n0]
        else:
            m0 = add_next_node(self)
            m1 = add_next_node(self)
            node_cache.setdefault(n0, {})[n1] = m0
            node_cache.setdefault(n1, {})[n0] = m1
            # add 3 spoke edges:
            self.add_edge(n0, m0)
            self.add_edge(m0, m1)
            self.add_edge(m1, n1)
        return m0, m1

    def set_spherical_positions(self, r=1, force=False):
        ''' 
        assign the "center" lat-lon coords to the soccerball graph. The "center"
        coords are x,y,z coords of the center of the face.  For "pent" faces, we
        can just hard-code these values.  For "hex" faces, we find the surrounding 
        three pent faces and then find the center of that triangle (in lat-lon coords).
        '''
        if 'latlon' in self.nodes[1] and not force:
            return
        pole = np.deg2rad(90.0)
        lat = math.atan(0.5)
        lon = np.deg2rad(36.0)

        # set spherical coords for pent vertices:
        self.nodes[1]['latlon'] = (pole, 0)
        self.nodes[2]['latlon'] = (lat, 0*lon)
        self.nodes[3]['latlon'] = (lat, 2*lon)
        self.nodes[4]['latlon'] = (lat, 4*lon)
        self.nodes[5]['latlon'] = (lat, 6*lon)
        self.nodes[6]['latlon'] = (lat, 8*lon)
        self.nodes[7]['latlon'] = (-lat, 1*lon)
        self.nodes[8]['latlon'] = (-lat, 3*lon)
        self.nodes[9]['latlon'] = (-lat, 5*lon)
        self.nodes[10]['latlon'] = (-lat, 7*lon)
        self.nodes[11]['latlon'] = (-lat, 9*lon)
        self.nodes[12]['latlon'] = (-pole, 0)

        # for p0, p1, p2 in self.pent_triples():
        #     import pdb; pdb.set_trace()
        #     hn = common_nodes(self, p0, p1, p2)[0]

        # report:
        for n in self.nodes:
            node = self.nodes[n]
            if 'latlon' in node:
                node['pos'] = sphr2cart(r, *node['latlon'])
            else:
                print(F"no 'latlon' in node {n}: edges={self.edges(n)}")

    @property
    def pent_edges(self):
        '''
        Return all the pent-pent "neighbors".  These are not actual
        edges in the graph because pent faces are not connected directly.
        These edges also correspond to the edges of a isosachrehedon.
        '''
        return IsoVertexGraph.edges

    def pent_triples(self):
        '''
        Return an iterable containing all the 3-tuples in the graph
        where each 3-tuple is a "connected" set of pent-vertices.
        '''
        vg = IsoVertexGraph()
        return n3s(vg)

    def pent_neighbors(self, p):
        ''' Given a pent face, return an ordered list of (hex) neighbors.
        Order is "ring" around pent face.
        '''
        if not self.is_pent_node(p):
            raise ValueError(p)
        hex_nodes = list(self.neighbors(p))
        assert len(hex_nodes) == 5

        n0 = hex_nodes.pop()
        neighbors = OrderedDict()
        neighbors[n0] = n0

        while True:
            n2 = common_nodes(self, p, n0)
            assert len(n2) == 2
            n0 = n2.pop()
            if n0 in neighbors:
                n0 = n2.pop()
            if n0 in neighbors:
                break
            neighbors[n0] = n0
        answer = neighbors.keys()
        assert len(answer) == 5
        return answer

    def face_verts(self):
        '''
        For the soccerball sphere, assign 3D coords to each vertex.
        Each vertex is shared by three faces (each face is a node in the graph)
        
        For each pent_edge, there are two vertices that can be found by
        tri-secting the line between the two center points of each pentagon.
        '''
        self.set_spherical_positions()
        
        import pdb; pdb.set_trace()
        for n0, n1, n2 in ng3(self):
            if not all([self.is_pent_node(n) for n in (n0, n1, n2)]):
                continue
            hex_node = common_nodes(self, n0, n1, n2)

        for f0, f1 in self.pent_edges:
            p0 = self.nodes[f0]['pos']
            p1 = self.nodes[f1]['pos']
            midpoints = divide_line(p0, p1, 3)
            # midpoints[1] and [2] are what we're after; need to determine
            # which faces (2) to add them to...
            # print(F"{f0}-{f1}: midpoints={midpoints}")
