'''
IsoVertexGraph class.
'''
import math
import networkx as nx


class IsoVertexGraph(nx.Graph):
    edges = (
        (1,2), (1,3), (1,4), (1,5), (1,6),
        (2,3), (2,6), (2,7), (2,8),
        (3,4), (3,8), (3,9),
        (4,5), (4,9), (4,10),
        (5,6), (5,10), (5,11),
        (6,7), (6,11),
        (7,8), (7,11), (7,12),
        (8,9), (8,12),
        (9,10), (9,12),
        (10,11), (10,12),
        (11,12),
    )

    def __init__(self, *args, **kwargs):
        '''
        Return a networkx graph where each node is the vertex of an icosahedron.

        Note:
        This graph has 12 nodes and 32 edges.
        '''
        nx.Graph.__init__(self, *args, **kwargs)
        self.add_edges_from(IsoVertexGraph.edges)

    def set_flat_positions(self, r):
        # manually set vertex positions:
        root3r2 = math.sqrt(3) * 3 * r / 2
        self.nodes[1]['pos'] = (6*r, -root3r2)

        self.nodes[2]['pos'] = (0*r, 0)
        self.nodes[3]['pos'] = (3*r, 0)
        self.nodes[4]['pos'] = (6*r, 0)
        self.nodes[5]['pos'] = (9*r, 0)
        self.nodes[6]['pos'] = (12*r, 0)

        self.nodes[7]['pos'] = (3/2*r, root3r2)
        self.nodes[8]['pos'] = (9/2*r, root3r2)
        self.nodes[9]['pos'] = (15/2*r, root3r2)
        self.nodes[10]['pos'] = (21/2*r, root3r2)
        self.nodes[11]['pos'] = (27/2*r, root3r2)

        self.nodes[12]['pos'] = (15/2*r, 2 * root3r2)


