'''
DEPRECATED in favor of iso_vertex_graph and iso_face_graph.

Model a sphere using subdivided hexagons.
'''

import math
import networkx as nx


class IsoVertexGraph(nx.Graph):
    def __init__(self, *args, **kwargs):
        '''
        Return a networkx graph where each node is the vertex of an icosahedron.

        Note:
        This graph has 12 nodes and 32 edges.
        '''
        nx.Graph.__init__(self, *args, **kwargs)
        edges = (
            (1,2), (1,3), (1,4), (1,5), (1,6),
            (2,3), (2,6), (2,7), (2,11),
            (3,4), (3,7), (3,8),
            (4,5), (4,8), (4,9),
            (5,6), (5,9), (5,10),
            (6,10), (6,11),
            (7,8), (7,11), (7,12),
            (8,9), (8,12),
            (9,10), (9,12),
            (10,11), (10,12),
            (11,12),
        )
        self.add_edges_from(edges)

    def set_flat_positions(self, r):
        # manually set vertex positions:
        root3r2 = math.sqrt(3) * 3 * r / 2
        self.nodes[1]['pos'] = (6*r, -root3r2)

        self.nodes[2]['pos'] = (0*r, 0)
        self.nodes[3]['pos'] = (3*r, 0)
        self.nodes[4]['pos'] = (6*r, 0)
        self.nodes[5]['pos'] = (9*r, 0)
        self.nodes[6]['pos'] = (12*r, 0)

        self.nodes[7]['pos'] = (3/2*r, root3r2)
        self.nodes[8]['pos'] = (9/2*r, root3r2)
        self.nodes[9]['pos'] = (15/2*r, root3r2)
        self.nodes[10]['pos'] = (21/2*r, root3r2)
        self.nodes[11]['pos'] = (27/2*r, root3r2)

        self.nodes[12]['pos'] = (15/2*r, 2 * root3r2)


class IsoFaceGraph(nx.Graph):
    '''
    Graph of connected faces on the iso-soccer ball.
    '''
    def __init__(self, *args, **kwargs):
        nx.Graph.__init__(self, *args, **kwargs)

    @classmethod
    def from_iso_vertex_graph(cls):
        '''
        Given the iso vertex graph, construct the iso face graph.
        Resulting graph has 32 nodes.  Each node has either 5 or 6 edges, and is
        center of a face of the soccer-ball tiling.
        '''
        iso = IsoVertexGraph()
        iso.set_flat_positions(1)
    
        f = cls()
        f.add_nodes_from(iso)
        i = iso.number_of_nodes() + 1

        # for each "triangle" in v, add a "center" hex node and connect it to each
        # vertex node in the triangle:
        for n0, n1, n2 in n3s(iso):
            f.add_node(i)
            f.add_edge(i, n0)
            f.add_edge(i, n1)
            f.add_edge(i, n2)
            i += 1

        # for each edge in v, connect two hex nodes that span the edge:
        for n0, n1 in iso.edges():
            # adding nodes is idempotent in nx
            # find the two nodes that are connected to n0 and n1:
            n0_neighbors = set(f.neighbors(n0))
            n1_neighbors = set(f.neighbors(n1))
            f_nodes = n0_neighbors.intersection(n1_neighbors)
            if len(f_nodes) == 2:
                f.add_edge(*f_nodes)
        return f

    def is_hex_node(self, n):
        return n > 12

    def is_pole_node(self, n):
        return n == 1 or n == 12

    def set_flat_positions(self, r):
        '''
        Set all node coords for the "flat" graph.
        '''
        # manually set vertex positions:
        root3r2 = math.sqrt(3) * 3 * r / 2
        self.nodes[1]['pos'] = (6*r, -root3r2)

        self.nodes[2]['pos'] = (0*r, 0)
        self.nodes[3]['pos'] = (3*r, 0)
        self.nodes[4]['pos'] = (6*r, 0)
        self.nodes[5]['pos'] = (9*r, 0)
        self.nodes[6]['pos'] = (12*r, 0)

        self.nodes[7]['pos'] = (3/2*r, root3r2)
        self.nodes[8]['pos'] = (9/2*r, root3r2)
        self.nodes[9]['pos'] = (15/2*r, root3r2)
        self.nodes[10]['pos'] = (21/2*r, root3r2)
        self.nodes[11]['pos'] = (27/2*r, root3r2)

        self.nodes[12]['pos'] = (15/2*r, 2 * root3r2)

        # hex nodes
        X, Y = 0, 1
        for node in range(13, 33):
            pent_neighbors = [n for n in self.neighbors(node) if not self.is_hex_node(n)]
            assert len(pent_neighbors) == 3

            # find two horizontal neighbors, then :
            n0, n1, n2 = pent_neighbors
            # import pdb; pdb.set_trace()
            if self.nodes[n0]['pos'][Y] == self.nodes[n1]['pos'][Y]:
                hpair = (n0, n1)
                v2 = n2
            elif self.nodes[n0]['pos'][Y] == self.nodes[n2]['pos'][Y]:
                hpair = (n0, n2)
                v2 = n1
            else:
                hpair = (n1, n2)
                v2 = n0

            # X coord is between hpair:
            y_offset = r/2.0
            h0, h1 = hpair
            cx = (self.nodes[h0]['pos'][X] + self.nodes[h1]['pos'][X]) / 2.0
            if self.nodes[v2]['pos'][Y] > self.nodes[h0]['pos'][Y]:
                cy = self.nodes[h0]['pos'][Y] + y_offset
            else:
                cy = self.nodes[h0]['pos'][Y] - y_offset

            self.nodes[node]['pos'] = (cx, cy)

            # h = groupBy(pent_neighbors, lambda n: self.nodes[n]['pos'][Y])
            # assert len(h) == 2
            # h_neighbors = h[0] if len(h[0]) == 2 else h[1]
            # v_neighbor = h[0] if len(h[0]) == 1 else h[1]
            # cx = v_neighbor['pos'][X]
            # dy = r / 4.0
            # if v_neighbor['pos'][Y] < h_neighbors[0]['pos'][Y]:
            #     dy = -dy
            # cy = h_neighbors[0]['pos'][Y] + dy
            # self.nodes[node]['pos'] = (cx, cy)


def n3s(g):
    '''
    Iterator: yield all triples (n0, n1, n2) such that nodes n0, n1, and n2
    are all mutually connected (forming a "triangle").
    '''
    cache = set()
    for n0, n1 in g.edges():
        nn0 = set(g.neighbors(n0))
        nn1 = set(g.neighbors(n1))
        for n2 in nn0.intersection(nn1):
            tri = tuple(sorted([n0, n1, n2]))
            if tri not in cache:
                yield tri
            cache.add(tri)
