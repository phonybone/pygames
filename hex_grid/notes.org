* Useful links
Wraparound hexagon tile maps on a sphere:
https://www.redblobgames.com/x/1640-hexagon-tiling-of-sphere/

Uber's H3:
https://eng.uber.com/h3/

How to calc. the angle between two vectors:
https://stackoverflow.com/questions/2827393/angles-between-two-n-dimensional-vectors-in-python

Short answer:
import numpy as np
# define three points that make up the vector (I'm assuming p1 is the "shared" point...)
p0 = [3.5, 6.7]
p1 = [7.9, 8.4]
p2 = [10.8, 4.8]

v0 = np.array(p0) - np.array(p1)
v1 = np.array(p2) - np.array(p1)
angle = np.math.atan2(np.linalg.det([v0,v1]),np.dot(v0,v1))

StackOverflow on how to tile a sphere with hexagons:
https://stackoverflow.com/questions/46777626/mathematically-producing-sphere-shaped-hexagonal-grid
