'''
Generate some SVG partial hexagons using HexGraph.
'''
from hex_svg import svg_dwg_start, svg_dwg_finish
from hex_graph import HexGraph


def test_hex_graph():
    dwg, base_group = svg_dwg_start()
    for start in range(1, 7):
        for end in range(start, 7):
            hex = HexGraph(cx=start*100, cy=end*100, rad=25)
            hex.svg(dwg, base_group, sectors=range(start, end+1))

    group = dwg.g(transform=F"translate(100, 700)")
    dwg.add(group)
    hex.svg(dwg, group, (1, 3, 5, 6))
    svg_dwg_finish(dwg)
