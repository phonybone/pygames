from iso_face_graph import IsoFaceGraph


def test_pent_neighbors():
    iso = IsoFaceGraph.from_iso_vertex_graph()
    print()
    for p in iso.pent_nodes:
        ring = list(iso.pent_neighbors(p))
        # print(F"ring({p}): {ring}")
        assert len(ring) == 5
        assert set(ring) == set(iso.neighbors(p))

        for i in range(5):
            left = ring[(i-1) % 5]
            mid = ring[i]
            right = ring[(i+1) % 5]
            assert (left, mid) in iso.edges
            assert (mid, right) in iso.edges
