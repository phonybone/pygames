'''
Test IsoFaceGraph.subdivide()
'''
from iso_face_graph import IsoFaceGraph


def test_subdivide():
    '''
    Test that the first subdivision of the soccer ball has...
    '''
    iso = IsoFaceGraph.from_iso_vertex_graph()
    iso.set_flat_positions(r=100)

    iso2 = iso.subdivide()
    n_pents = 0
    for n in iso2.nodes:
        if iso2.is_pent_node(n):
            assert len(list(iso2.neighbors(n))) == 5
            n_pents += 1
        else:
            assert len(list(iso2.neighbors(n))) == 6
    assert n_pents == 12

    # do it again:
    iso3 = iso2.subdivide()
    n_pents = 0
    for n in iso3.nodes:
        if iso3.is_pent_node(n):
            assert len(list(iso3.neighbors(n))) == 5
            n_pents += 1
        else:
            assert len(list(iso3.neighbors(n))) == 6
    assert n_pents == 12
