'''

'''
from iso_face_graph import IsoFaceGraph


def test_set_spherical():
    print()
    iso = IsoFaceGraph.from_iso_vertex_graph()
    iso.set_spherical_positions()

    def trim(floats, prec):
        '''
        for a given iterable of floats and a preceision value, return a generator of
        string representations of the floats using the given precision.
        '''
        return tuple((F"{float(f):.{prec}}" for f in floats))

    for n in iso.nodes:
        if iso.is_pent_node(n):
            print(F"{n:2}: spos={trim(iso.nodes[n]['spos'], 3)}    pos={trim(iso.nodes[n]['pos'], 3)}")

