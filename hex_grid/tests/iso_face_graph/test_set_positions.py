from iso_face_graph import IsoFaceGraph
from utils import bounding_box
from hex_svg import svg_dwg_start, svg_dwg_finish, svg_line_graph
from hex_graph import HexGraph


def test_set_flat_positions():
    rad = 100
    f = IsoFaceGraph.from_iso_vertex_graph()
    f.set_flat_positions(rad)

    # for node in f:
    #     print(F"{node}: {f.nodes[node]['pos']}")
    bb = bounding_box(f)
    dwg, base_group = svg_dwg_start(bounding_box=bb)
    svg_line_graph(dwg, base_group, f, draw_edges=True)

    # draw some hexs:
    for n in f.nodes:
        cx, cy = f.nodes[n]['pos']
        try:
            sectors = f.nodes[n]['sectors']
        except KeyError:
            sectors = None
        # hex = HexGraph(rad=rad, cx=cx, cy=cy)
        # hex.svg(dwg, base_group, sectors=sectors)

    svg_dwg_finish(dwg)
