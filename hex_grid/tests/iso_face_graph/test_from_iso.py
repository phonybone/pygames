from iso_face_graph import IsoFaceGraph


def test_from_iso():
    f = IsoFaceGraph.from_iso_vertex_graph()
    print(F"face graph: {f.number_of_nodes()} nodes, {f.number_of_edges()} edges")

    for n in f.nodes:
        print(F"testing {n}")
        if f.is_hex_node(n):
            assert len(list(f.neighbors(n))) == 6
        else:
            assert len(list(f.neighbors(n))) == 5
    assert f.number_of_nodes() == 32
    assert f.number_of_edges() == 90

def test_neighbors():
    f = IsoFaceGraph.from_iso_vertex_graph()

    print("f neighbors:")
    for node in f:
        print(F"node {node}: neighbors: {list(f.neighbors(node))}")
