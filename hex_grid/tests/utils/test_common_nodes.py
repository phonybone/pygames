from iso_face_graph import IsoFaceGraph
from utils import common_nodes

def test_common_nodes():
    iso = IsoFaceGraph.from_iso_vertex_graph()
    for hn in iso.hex_nodes:
        pents = [n for n in iso.neighbors(hn) if iso.is_pent_node(n)]
        cns = common_nodes(iso, *pents)
        print(F"hex_node {hn}: pents={pents} cns={cns}")

        assert len(pents) == 3
        assert len(cns) == 1
        assert cns.pop() == hn
