from iso_vertex_graph import IsoVertexGraph
from utils import n3s


def test_triangles():
    print("\nTriangles:")

    iso = IsoVertexGraph()
    for tri in n3s(iso):
        print(tri)
    
