from iso_vertex_graph import IsoVertexGraph


def test_init():
    iso = IsoVertexGraph()

    for node in iso:
        print(F"node {node}: neighbors: {list(iso.neighbors(node))}")

    print(F"{len(iso.nodes())} nodes")
    print(F"{len(iso.edges)} edges")

    print("Edges:")
    for n1, n2 in iso.edges:
        print(F"{n1}-{n2}: {iso[n1][n2]}")
    
