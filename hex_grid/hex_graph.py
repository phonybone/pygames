'''
HexGraph class: used for drawing hexagons and "partial" hexagons.
'''
import math
from copy import deepcopy
import networkx as nx
from hex_svg import svg_line_graph


class HexGraph(nx.Graph):
    def __init__(self, cx, cy, rad=1, *args, **kwargs):
        nx.Graph.__init__(self, *args, **kwargs)

        root3r2 = rad * math.sqrt(3)/2
        half = rad * 0.5
        self.add_nodes_from(range(7))
        self.nodes[0]['pos'] = (cx, cy)
        self.nodes[1]['pos'] = (cx + rad, cy)
        self.nodes[2]['pos'] = (cx + half, cy + root3r2)
        self.nodes[3]['pos'] = (cx - half, cy + root3r2)
        self.nodes[4]['pos'] = (cx - rad, cy)
        self.nodes[5]['pos'] = (cx - half, cy - root3r2)
        self.nodes[6]['pos'] = (cx + half, cy - root3r2)

    def svg(self, dwg, group, sectors=None):
        '''
        Return SVG describing a portion of a hex.

        sectors is a list of sectors to include.
        There can be up to six sectors.  Sectors are triangles
        that make up the interior of the hexagon.  They are
        numbered anti-clockwise staring with the upper right.
        '''
        edges = deepcopy(self.edges)
        self.remove_edges_from(edges)  # remove all edges
        n0 = 0
        if sectors is None:
            sectors = range(1, 7)
            
        for s in sectors:
            assert s >= 1 and s <= 6
            n1 = s
            n2 = (n1 % 6) + 1
            assert n2 >= 1 and n2 <= 6, n2
            self.add_edge(n1, n2)  # add "outside" edge

            if self.has_edge(n0, n1):  # check radial edge n0->n1
                self.remove_edge(n0, n1)
            else:
                self.add_edge(n0, n1)

            if self.has_edge(n0, n2):  # check radial edge n0->n2
                self.remove_edge(n0, n2)
            else:
                self.add_edge(n0, n2)

        svg_line_graph(dwg, group, self, draw_nodes=False)
