'''
routines for drawing svg hex graphs.
'''

import math
import svgwrite as svg
import xml.dom.minidom


from iso_vertex_graph import IsoVertexGraph
from iso_face_graph import IsoFaceGraph
from utils import bounding_box, unit_hex

RADIUS = 100


def svg_dwg_start(fn = 'hex.svg', stroke='black', stroke_width='1px', 
                    bounding_box=(0, 600, 0, 1000), pad=50):
    minx, maxx, miny, maxy = bounding_box
    width = maxx - minx + 2*pad
    height = maxy - miny + 2*pad
    left, bottom = minx-pad, miny-pad
    size = (width, height)
    # viewbox = center_viewbox(*size)
    viewbox = F"{left} {bottom} {width} {height}"
    print(F"viewbox: {viewbox}")

    dwg = svg.Drawing(filename=fn, size=size, debug=True, viewBox=viewbox)
    transform = f'scale(1 -1) translate(0 {height/2})'
    base_group = dwg.add(dwg.g(id='base',
                               stroke=stroke, stroke_width=stroke_width,
                               # transform=transform
                               ))
    # svg_axis(dwg, base_group, 100, 100, full=True)
    svg_border(dwg, base_group, (bottom, left), (width, height))
    return dwg, base_group


def svg_dwg_finish(dwg, fn='hex.svg'):
    dwg.save()

    # re-write file using toprettyxml():
    dom = xml.dom.minidom.parse(fn)
    with open(fn, 'w') as f:
        print(dom.toprettyxml(), file=f)
    print(F"{fn} written")


def svg_hex(dwg, group):
    ''' draw a unit hexagon at the origin '''
    uhex = unit_hex(RADIUS)
    coord0 = uhex[0]
    for coord1 in uhex[1:]:
        group.add(dwg.line(start=(coord0[0], coord0[1]), end=(coord1[0], coord1[1])))
        coord0 = coord1
    group.add(dwg.line(start=(uhex[-1][0], uhex[-1][1]), end=(uhex[0][0], uhex[0][1])))


def svg_hex_at(dmg, hid, x, y, color, sw='1px'):
    ''' draw a unit hexagon at a given location '''
    hex_group = dwg.add(dwg.g(id=hid, stroke=color, stroke_width=sw, transform=f'translate({x} {y})'))
    svg_hex(dwg, hex_group)


def svg_pentagon_at(dmg, hid, x, y, color, up, sw='1px'):
    ''' draw a partial hexagon (pentagon) at a given point '''
    hex_group = dwg.add(dwg.g(id=hid, stroke=color, stroke_width=sw, transform=f'translate({x} {y})'))
    svg_pentagon(dwg, hex_group, up)


def center_viewbox(height, width):
    '''
    Return (left, bottom, width, height) for a viewbox centered on height & width.
    '''
    return F"{-width/2} {-height/2} {width} {height}"


def svg_pentagon(dwg, group, up):
    uhex = unit_hex(RADIUS)
    coord0 = uhex[0]
    for i, coord1 in enumerate(uhex[1:]):
        if not (i == 4 and up or i == 1 and not up):
            group.add(dwg.line(start=(coord0[0], coord0[1]), end=(coord1[0], coord1[1])))
        coord0 = coord1
    group.add(dwg.line(start=(uhex[-1][0], uhex[-1][1]), end=(uhex[0][0], uhex[0][1])))

    if up:
        group.add(dwg.line(start=(0, 0), end=(uhex[4][0], uhex[4][1])))
        group.add(dwg.line(start=(0, 0), end=(uhex[5][0], uhex[5][1])))
    else:
        group.add(dwg.line(start=(0, 0), end=(uhex[1][0], uhex[1][1])))
        group.add(dwg.line(start=(0, 0), end=(uhex[2][0], uhex[2][1])))


def svg_axis(dwg, group, height, width, full=False):
    group.add(dwg.line(start=(0, 0), end=(0, height), stroke="black"))
    group.add(dwg.line(start=(0, 0), end=(width, 0), stroke="black"))
    if full:
        group.add(dwg.line(start=(0, 0), end=(0, -height), stroke="black"))
        group.add(dwg.line(start=(0, 0), end=(-width, 0), stroke="black"))
        

def svg_border(dwg, group, botleft, wh, pad=0):
    '''
    bot_eft: tuple (bottom, left)
    wh: tuple(width, height)
    '''
    bottom, left = botleft
    bottom -= pad
    left -= pad
    width, height = wh
    top, right = bottom + height, left + width
    top += pad
    right += pad
    group.add(dwg.line(start=(left, bottom), end=(right, bottom)))
    group.add(dwg.line(start=(left, bottom), end=(left, top)))
    group.add(dwg.line(start=(right, top), end=(left, top)))
    group.add(dwg.line(start=(right, top), end=(right, bottom)))


def svg_line_graph(dwg, group, graph, draw_edges=True, draw_nodes=True, label_nodes=True):
    '''
    Draw a circles and lines graph.
    Assumes all nodes in g have a ['pos']=(x, y) attribute.
    '''
    n_rad = 1

    # edges:
    if draw_edges:
        for n0, n1 in graph.edges:
            try:
                pos0 = graph.nodes[n0]['pos']
                pos1 = graph.nodes[n1]['pos']
            except KeyError:
                continue
            group.add(dwg.line(start=pos0, end=pos1, stroke='black', stroke_width='1'))

    # nodes:
    if draw_nodes:
        for n in graph.nodes:
            try:
                center = graph.nodes[n]['pos']
                group.add(dwg.circle(center=center, r=n_rad, stroke='black', stroke_width='5', fill='white'))
                if label_nodes:
                    group.add(dwg.text(str(n), center))
            except KeyError:
                continue

if __name__ == '__main__':
    iso = IsoVertexGraph()
    iso.set_flat_positions(r=30)
    vgraph = IsoFaceGraph.from_iso_vertex_graph()
    vgraph.set_flat_positions(r=50)

    fn = 'hex.svg'
    minx, maxx, miny, maxy = bounding_box(vgraph)
    pad = 50
    width = maxx - minx + 2*pad
    height = maxy - miny + 2*pad
    left, bottom = minx-pad, miny-pad
    stroke = 'black'
    stroke_width = '1px'
    size = (width, height)
    # viewbox = center_viewbox(*size)
    viewbox = F"{left} {bottom} {width} {height}"
    print(F"viewbox: {viewbox}")

    dwg = svg.Drawing(filename=fn, size=size, debug=True, viewBox=viewbox)
    transform = f'scale(1 -1) translate(0 {height/2})'
    base_group = dwg.add(dwg.g(id='base',
                               stroke=stroke, stroke_width=stroke_width,
                               # transform=transform
                               ))
    # svg_axis(dwg, base_group, 100, 100, full=True)
    svg_border(dwg, base_group, (bottom, left), (width, height))

    # svg_hex_at(dwg, 1, 100, 100, 'green', '1')
    # svg_pentagon_at(dwg, 1, 100, -100, 'red', True, '1')

    # svg_line_graph(dwg, base_group, iso)
    svg_line_graph(dwg, base_group, vgraph)
