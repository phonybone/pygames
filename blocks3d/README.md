The idea:

A minecraft-like block system defining a world (with or without gravity).  Spaces/cells
can be empty or contain a block.  Use a hash w/3D keys to determine if a block exists
at a given cell or not.  Have autonomous agents perform operations on blocks, and in
response to block configurations in a manner similar, but more complicated than, Conway's
Life; ie, a rule-bases system where each actor has it's own (evolved?) set of rules.