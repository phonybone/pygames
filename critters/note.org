* Evolutionary Critters
Critters have:
- energy

- functions that perform actions, cost energy, maybe have prerequisites, and have effects on both the critter and the environment.
-- move
--- hunt
--- flee
-- eat
-- mate
-- grow
-- sleep
-- call (ie, make sounds)
-- emit chemicals
-- fight/attack/defend/warn/submit

- state: affects parameters of action functions
-- size
-- speed
-- metabolism (effects speed/movement, et al?)

- input/senses:
-- listen to sounds
-- sniff odors
-- taste possible foods
-- touch
-- see
-- others?

Critters have to interact with their environment and each other.

Critters have to have a string-encodable genome that governs their entire behavoir.  This may be the structure and weights
of a nn.

Critters might be plants that have no movement, but can get energy "for free".

Interactions between organisms (of all kinds) have to be more complex than what is 
described here so far, else the evolutionary min/maxes will be too easy to attain.

