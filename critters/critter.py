'''
Notes:
Critter's action is determined by an NN where 
inputs are the critter's interal attributes (health, energy,
everything in the constructor basically) and the surrounding
terrain.

Each action has side-effects on the critter (and possibly
on critters that it interacts with), and a cost.  Basic
critter vitality is measured as a combination of energy
and health.

Each critter's NN is built by inspecting the critter class
and the terrain class to determine the number of input and 
output nodes of the NN.  There is one output node for each
critter @action, plus one output node for each needed arg
of the actions.  The action selected is the action output
node with the highest value (after sigmoid).

Critters will pass on their characteristics by mating
and giving birth, both of which will also be actions.

The NN's will have the potential to be fully connected,
possibly multi-layered (but not initially).

NN's must also have the ability to mutate on their own;
mutations may take the form of new connections between nodes, 
or modifications of weights.


'''

import random
from pbgames.physics.numerics import logistic_growth

actions = {}
def action(method):
    ''' action decorator '''
    actions[method.__name__] = method
    return method

    
class Critter:
    def __init__(self):
        self.dead = False
        # self.pos = Position()
        self.energy = 0
        self.plant_metabolism = 1
        self.meat_metabolism = 1
        self.gender = random.choice(['m', 'f'])
        
        self.size = 0
        self.attack = 0
        self.defense = 0
        self.speed = 0
        self.health = 0
        
        # self.actions = []
        # self.health = 0         # how/why?
        # self.genome = Genome()  # for later

    def turn(self, tcell):
        if self.dead:
            return
        action, args = self.decide(tcell)  # invokes NN
        cost = self.action(*args)     # +side-effects
        self.energy -= cost
        self.energy -= self.heal()  # every critter gets a heal action
        if self.energy < 0:
            self.dead = True

    # all actions return an energy cost.

    def decide(self, tcell):
        # have to decide on NN representation
        # run NN on inputs(self, tcell)
        # choose action by node of highest value
        # get associated args if any
        # return action function
        pass

    @action
    def graze(self, cell):
        if cell.vegetation >= self.eat_rate:
            cell.vegetation -= self.eat_rate
            self.energy = self.eat_rate * self.plant_metabolism

    @action
    def move(self, direction, speed):
        '''
        direction is one of eight choices: n, nw, w, etc
        energy consumed = self.move_eff * speed * speed * size

        how to choose direction and speed?  NN chooses, but how?
        direction and speed could be outputs of the NN
        '''
        pass

    @action
    def grow(self):
        self.size *= self.growth_rate
        return self.size * self.growth_cost

    @action
    def heal(self):
        self.health = logistic_growth(self.health)
        return self.heal_cost

    @action
    def attack(self, other):
        self.health -= other.defense * other.size
        if self.health < 0:
            self.dead = True
        other.health -= self.attack * self.size
        if other.health < 0:
            other.dead = True
        return self.attack_cost

    @action
    def defend(self, other):
        other.health -= self.defense * self.size
        if other.heath < 0:
            other.dead = True
        self.health -= other.attack * other.size
        if self.health < 0:
            self.dead = True
        return self.defense

    @action
    def mate(self, other):
        # hmm....
        pass

    @action
    def scavenge(self, other):
        assert other.dead
        lbs_meat = min(other.size, other.size * self.size * self.scavenge_rate)
        other.size -= lbs_meat
        self.energy += lbs_meat * self.meat_metabolism
        
