'''
Rather than try to define all critter functionality in one big
class, make a Critter class that uses mixins to add functionality.
'''

class BlockCritter:
    @classmethod
    def add_mixin(cls, mixin_cls):
        new_cls_name = cls.__name__ + '-' + mixin_cls.prefix
        cls_attrs = {}
        for key, attr in vars(mixin_cls).items():
            if not key.startswith('_'):
                cls_attrs[key] = attr
        cls_attrs[mixin_cls.prefix+'__init__'] = mixin_cls.__init__
        # print(cls_attrs)
        return type(new_cls_name, (cls,), cls_attrs)

    # def __init__(self):
    #     print(F"creating {self.__class__.__name__}")

if __name__ == '__main__':
    from energetic import Energetic
    from mobile import Mobile
    BC_E = BlockCritter.add_mixin(Energetic)
    BC_E_M = BC_E.add_mixin(Mobile)

    bug = BC_E_M()
    Energetic.__init__(bug, 5, 100)
    Mobile.__init__(bug, 0, 0, 1, 1, 0.1)
    import json
    print(json.dumps(vars(bug), indent=2))
    bug.move(0.5, 0.5)
    print(json.dumps(vars(bug), indent=2))
