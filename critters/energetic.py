class Energetic:
    prefix = 'ener'
    
    ''' Mixing for anything that manages it's own energy '''
    def __init__(self, energy, max_energy):
        self.energy = energy
        self.max_energy = max_energy

    def energy_spend(self, energy_cost):
        ''' do something that uses energy; energy_cost < 0 allowed (eg, eating) '''
        self.energy -= energy_cost
        self.energy = min(self.energy, self.max_energy)
        if self.energy < 0:
            self.die()

    def die(self):
        raise RuntimeError(self, 'critter died')
