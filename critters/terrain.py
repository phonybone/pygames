'''
Critters live in a Terrain.  A Terrain is a grid of terrain types, each of which
have various characteristics.
'''

from functools import partial
import random

from pbgames.physics.grid import Grid
from pbgames.physics.grid import grid_idxs
from pbgames.physics.numerics import logistic_growth


class TerrainCell:
    def __init__(self, v0, vgr, vcc):
        '''
        v0: initial vegetation content
        vgr: vegetative grown rate
        vcc: vegetative carrying capacity (eg max vegetation)
        '''
        self.vegetation = v0    # basic vegetation content

        # set growth function
        self.vgr = vgr
        self.vcc = vcc
        self.growth = partial(logistic_growth, r=vgr, cc=vcc)

    def __str__(self):
        return F"v0={self.vegetation:.2f}, vcc={self.vcc:.2f}, vgr={self.vgr:.5f}"

    def grow(self):
        self.vegetation = self.growth(self.vegetation)


def random_cell(i, j):
    return TerrainCell(random.random()*10,
                       random.random()*0.002,
                       random.random()*100,)


def my_cell(i, j):
    ''' hard-coding values for debug purposes '''
    return TerrainCell(10, 0.2, 100)


class Terrain:
    '''
    2D array of TerrainCells
    '''
    def __init__(self, rows, cols):
        self.rows = rows
        self.cols = cols
        self.grid = Grid(rows, cols, TerrainCell, my_cell)

    def cell_at(self, i, j):
        return self.grid[i][j]

    def grow(self):
        ''' apply growth function to all TCs '''
        for i, j in grid_idxs(self.rows, self.cols):
            self.cell_at(i, j).grow()
