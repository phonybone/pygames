from math import sqrt

class Mobile:
    prefix = 'mobile'

    ''' mixing to add mobility functionality '''
    def __init__(self, x, y, dx, dy, cost_max_move):
        self.x = x
        self.y = y
        self.dx = dx
        self.dy = dy
        self.max_move_cost = cost_max_move  # cost to move full (dx, dy)

        # fixed properties:
        self.max_move = sqrt(self.dx * self.dx + self.dy + self.dy)

    def move(self, dx, dy):
        if dx > self.dx or dy > self.dy:
            raise ValueError(f"Movement too great: {dx} > {self.dx} or {dy} > {self.dy}")
        self.x += dx
        self.y += dy
        move = sqrt(dx * dx + dy * dy)

        energy_cost = (move / self.max_move) * self.max_move_cost
        self.energy_spend(energy_cost)
