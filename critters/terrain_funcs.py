'''
Continuous world.

Spot-functions for various terrain characteristics.
(May contain stubs/WIPs)
'''
import math

def elevation(lat, lon):
    ''' meters above/below sea level '''
    # later we'll generate or load a map of static values
    return 0

def sunlight(lat, lon, h):
    '''
    baseline sunlight (W/m^2) level based on location and time of day/year.
    -180 <= lon <= 180
    -90 <= lat <= 90
    h is seconds since the epoch
    
    Does not (yet) account for seasons
    '''
    # this function should be memoized!
    max_sl = 1360               # W/m^2
    tod = int(h) % 86400
    return max(0, max_sl * math.sin(math.radians(tod/240 + lon)) * math.cos(math.radians(lat)))

def temp(lat, lon, h):
    '''
    '''
    # ambient temp based on latitude
    # 
    pass

if __name__ == '__main__':
    def test_sunlight():
        for lat, lon in zip(range(-90, 90, 10), range(-180, 180, 20)):
            sls = '  '.join(f"{sunlight(lat, lon, h):6.1f}" for h in range(0, 86401, 3600))
            print(F"lat {lat:4}: {sls}")

