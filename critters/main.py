#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''
Critters

Author: Victor Cassen
vmc.swdev@gmail.com
'''


import random
from pbutils.argparsers import parser_stub, wrap_main
import pbgames.colors as colors
from critter_game import CritterGame


def main(config):
    random.seed(0)
    size = (int(config.width), int(config.height))
    game = CritterGame(size, colors.black)
    game.loop()
    print('yay')
    return 0


def make_parser():
    parser = parser_stub(__doc__)
    # parser.add_argument('--', help='')
    parser.add_argument('arg', nargs='*')

    return parser


if __name__ == '__main__':
    parser = make_parser()
    wrap_main(main, parser) 
