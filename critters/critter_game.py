from pbgames.gamebase import Game
from critters.terrain import Terrain


class CritterGame(Game):
    def __init__(self, size, background, fps=24):
        super(CritterGame, self).__init__(size, background, fps)
        self.n_gen = 0

    def init_game(self):
        self.terrain = Terrain(*self.size)

    def update(self):
        self.terrain.grow()
        self.n_gen += 1
        return self.n_gen < 30
