'''
'''


class Card:
    def __init__(self, cardno):
        self.rank = cardno % 13
        self.suit = cardno // 13

    c2ranks = ['A', '2', '3', '4', '5', '6', '7', '8', '9', 'T', 'J', 'Q', 'K']
    c2suits = ['\u2660', '\u2665', '\u2666', '\u2663']

    def __str__(self):
        return f"{self.c2ranks[self.rank]}{self.c2suits[self.suit]}"

    def __repr__(self):
        return f"r={self.rank} s={self.suit}"

    @property
    def deckno(self):
        return self.suit * 13 + self.rank

    rank2c = {'A': 0, 'T': 9, 'J': 10, 'Q': 11, 'K': 12}
    suit2c = {'S': 0, 'H': 1, 'D': 2, 'C': 3}

    @classmethod
    def parse(cls, token):
        r, s = list(token)
        if r in cls.rank2c:
            rank = cls.rank2c[r]
        else:
            rank = int(r) - 1

        return cls(cls.suit2c[s] * 13 + rank)


class Hand:
    def __init__(self, c1, c2, c3, c4, c5):
        self.c1 = c1
        self.c2 = c2
        self.c3 = c3
        self.c4 = c4
        self.c5 = c5

    def __str__(self):
        return ' '.join(map(str, self.hand))

    @property
    def hand(self):
        return [self.c1, self.c2, self.c3, self.c4, self.c5]

    HIGHCARD = 1
    PAIR = 2
    TWO_PAIR = 3
    THREE_KIND = 4
    STRAIGHT = 5
    FLUSH = 6
    FULLHOUSE = 7
    FOUR_KIND = 8
    STRAIGHT_FLUSH = 9

    SCORE_NAMES = [
        '',
        'High card',
        'Pair',
        'Two Pair',
        'Three of a Kind',
        'Straight',
        'Flush',
        'Full House',
        'Four of a Kind',
        'Straight Flush',
    ]

    @classmethod
    def scorename(cls, score):
        return cls.SCORE_NAMES[score]

    def collect_pairs(self):
        '''
        Examine a hand to determine
        '''
        c1, c2, c3, c4, c5 = self.hand

        highcard = c1.rank
        lowcard = c1.rank
        flush_suit = c1.suit

        pair1, pair2 = None, None
        threekind = None
        fourkind = None

        if c2.rank == c1.rank:
            pair1 = c1.rank
        highcard = max(highcard, c2.rank)
        lowcard = min(lowcard, c2.rank)
        if c2.suit != c1.suit:
            flush_suit = None

        # c3
        if c3.rank == pair1:
            threekind = c3.rank
            pair1 = None
        elif c3.rank == c1.rank:
            pair1 = c1.rank
        elif c3.rank == c2.rank:
            pair1 = c2.rank
        if c2.suit != flush_suit:
            flush_suit = None
        highcard = max(highcard, c3.rank)
        lowcard = min(lowcard, c3.rank)
        if flush_suit != c3.suit:
            flush_suit = None

        # c4
        if c4.rank == threekind:
            fourkind = threekind
            threekind = None
        elif c4.rank == pair1:
            threekind = pair1
            pair1 = None
        elif c4.rank == pair2:      # not sure this case can happen...
            threekind = pair2
            pair2 = None
        elif c4.rank == c1.rank:
            if pair1 is None:
                pair1 = c4.rank
            else:
                pair2 = c4.rank
        elif c4.rank == c2.rank:
            if pair1 is None:
                pair1 = c4.rank
            else:
                pair2 = c4.rank
        elif c4.rank == c3.rank:
            if pair1 is None:
                pair1 = c4.rank
            else:
                pair2 = c4.rank
        highcard = max(highcard, c4.rank)
        lowcard = min(lowcard, c4.rank)
        if flush_suit != c4.suit:
            flush_suit = None

        # c5
        if c5.rank == threekind:
            fourkind = threekind
            threekind = None
        elif c5.rank == pair1:
            threekind = pair1
            pair1 = None
        elif c5.rank == pair2:
            threekind = pair2
            pair2 = None
        elif c5.rank == c1.rank:
            if pair1 is None:
                pair1 = c5.rank
            else:
                pair2 = c5.rank
        elif c5.rank == c2.rank:
            if pair1 is None:
                pair1 = c5.rank
            else:
                pair2 = c5.rank
        elif c5.rank == c3.rank:
            if pair1 is None:
                pair1 = c5.rank
            else:
                pair2 = c5.rank
        elif c5.rank == c4.rank:
            if pair1 is None:
                pair1 = c5.rank
            else:
                pair2 = c5.rank
        highcard = max(highcard, c5.rank)
        lowcard = min(lowcard, c5.rank)
        if flush_suit != c5.suit:
            flush_suit = None

        return lowcard, highcard, pair1, pair2, threekind, fourkind, flush_suit

    def score(self, lowcard, highcard, pair1, pair2, threekind, fourkind, flush_suit):
        '''
        Return a score and kickers based on the results of collect_pairs()
        '''
        sorted_ranks = sorted([c.rank for c in self.hand])
        if (sorted_ranks == [0, 9, 10, 11, 12] or (highcard - lowcard == 4)) and \
           pair1 is None and \
           pair2 is None and \
           threekind is None and \
           fourkind is None:
            straight = True
        else:
            straight = False

        if straight and flush_suit:
            return self.STRAIGHT_FLUSH, (highcard,)

        if fourkind is not None:
            return self.FOUR_KIND, (fourkind, highcard)

        if threekind is not None and pair1 is not None:
            return self.FULLHOUSE, (threekind, pair1)
        if threekind is not None and pair2 is not None:
            return self.FULLHOUSE, (threekind, pair2)

        if flush_suit is not None:
            return self.FLUSH, (highcard, lowcard)  # actually need the inbetween cards, too

        if straight:
            return self.STRAIGHT, (highcard,)

        if threekind is not None:
            return self.THREE_KIND, ()

        if pair1 is not None and pair2 is not None:
            if pair1 > pair2:
                return self.TWO_PAIR, (pair1, pair2, highcard)
            else:
                return self.TWO_PAIR, (pair2, pair1, highcard)

        if pair1 is not None:
            return self.PAIR, (pair1, highcard)
        if pair2 is not None:
            return self.PAIR, pair2, highcard

        return self.HIGHCARD, (highcard,)

    @classmethod
    def parse(cls, hand_desc):
        return cls(*[Card.parse(c) for c in hand_desc.split(' ')])


if __name__ == '__main__':
    import itertools as it

    def test_cards():
        for s in range(4):
            for r in range(13):
                c = Card((s * 13) + r)
                assert repr(c) == f'r={r} s={s}'

    def test_parse():
        deckno = 0
        for s in 'SHDC':
            for r in Card.c2ranks:
                card = Card.parse(f'{r}{s}')
                assert card.deckno == deckno
                deckno += 1

    def test_collect_pairs():
        test_cases = (
            (Hand.parse('3H 3S 7C KD TH'), (2, 12, 2, None, None, None, None), Hand.PAIR),

            (Hand.parse('3H 3S 7C 7D TH'), (2, 9, 2, 6, None, None, None), Hand.TWO_PAIR),
            (Hand.parse('3H TH 3S 7C 7D'), (2, 9, 2, 6, None, None, None), Hand.TWO_PAIR),
            (Hand.parse('3H 3S 8D 7C 7C'), (2, 7, 2, 6, None, None, None), Hand.TWO_PAIR),
            (Hand.parse('3H 3S 7C QS 7D'), (2, 11, 2, 6, None, None, None), Hand.TWO_PAIR),
            (Hand.parse('9C 3H 3S 7C 7D'), (2, 8, 2, 6, None, None, None), Hand.TWO_PAIR),

            (Hand.parse('3H 3S 3C KD TH'), (2, 12, None, None, 2, None, None), Hand.THREE_KIND),
            (Hand.parse('3H 3S 3C KD 3D'), (2, 12, None, None, None, 2, None), Hand.FOUR_KIND),

            (Hand.parse('3H 3S 3C KD KS'), (2, 12, 12, None, 2, None, None), Hand.FULLHOUSE),
            (Hand.parse('3H 3S KC KD KS'), (2, 12, 2, None, 12, None, None), Hand.FULLHOUSE),

            (Hand.parse('3H 4H 5H 6H 7D'), (2, 6, None, None, None, None, None), Hand.STRAIGHT),
            (Hand.parse('3H 5H QH 8H AH'), (0, 11, None, None, None, None, 1), Hand.FLUSH),
            (Hand.parse('3H 4H 5H 6H 7H'), (2, 6, None, None, None, None, 1), Hand.STRAIGHT_FLUSH),
            (Hand.parse('3H 5H QH 8S AH'), (0, 11, None, None, None, None, None), Hand.HIGHCARD),
        )
        for hand, expected, exp_score in test_cases:
            bh = hand.collect_pairs()
            lc, hc, p1, p2, tk, fk, fs = bh
            try:
                assert bh == expected
            except AssertionError:
                print('FAIL')
                print(f'collect_pairs: lc={lc}, hc={hc}, p1={p1}, p2={p2}, tk={tk}, fk={fk}, fs={fs}')
                print('expected:      lc={}, hc={}, p1={}, p2={}, tk={}, fk={}, fs={}'.format(*expected))

            act_score, kickers = hand.score(*bh)
            try:
                assert act_score == exp_score
            except AssertionError:
                print(f"FAIL: hand = {hand}")
                print(f"act_score: {Hand.scorename(act_score)}, exp_score: {Hand.scorename(exp_score)}")

    def test_hands():
        hands = (
            (Hand.parse('KH 2H 3H 9C 8H'), Hand.HIGHCARD),
            (Hand.parse('AH 2H 3H 9C 8H'), Hand.HIGHCARD),
            (Hand.parse('AH 2H 3H 2C 8H'), Hand.PAIR),
            (Hand.parse('3S 3H 6S 6H 9C'), Hand.TWO_PAIR),
            (Hand.parse('3S 3H 3C 7S QD'), Hand.THREE_KIND),
            (Hand.parse('3S 3H 3C 3D QD'), Hand.FOUR_KIND),
            (Hand.parse('AS AH AC AD QD'), Hand.FOUR_KIND),

            (Hand.parse('AH 2D 3S 4S 5H'), Hand.STRAIGHT),
            (Hand.parse('3S 4D 5S 6H 7H'), Hand.STRAIGHT),
            (Hand.parse('9C TH JD QD KC'), Hand.STRAIGHT),
            (Hand.parse('TH JD QD KC AH'), Hand.STRAIGHT),

            (Hand.parse('2H 6H QH KH AH'), Hand.FLUSH),
            (Hand.parse('3S 3H 3C 7S 7D'), Hand.FULLHOUSE),
            (Hand.parse('TH JH QH KH AH'), Hand.STRAIGHT_FLUSH),
            (Hand.parse('AH 2H 3H 4H 5H'), Hand.STRAIGHT_FLUSH),
        )
        for hand, exp_score in hands:
            test_hand(hand, exp_score)
            print(hand, Hand.scorename(exp_score))

    def test_hand(hand, exp_score):
        for idxs in it.permutations(range(5)):
            bh = hand.collect_pairs()
            scr = hand.score(*bh)
            msg = f"hand={hand}, scr={Hand.scorename(scr[0])}, exp_score={Hand.scorename(exp_score)}, bh={str(bh)}"
            assert scr[0] is exp_score, msg

    test_cards()
    test_parse()
    test_collect_pairs()
    test_hands()
    print('yay')
