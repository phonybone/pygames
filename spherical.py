'''
Sandbox implementation of spherical coordinates.
'''
import math


def sphere2cart(r, theta, phi):
    '''
    convert r, theta, phi to x, y, z
    '''
    x = r * math.sin(theta) * math.cos(phi)
    y = r * math.sin(theta) * math.sin(phi)
    z = r * math.cos(theta)
    return x, y, z


def cart2sphere(x, y, z):
    r = math.sqrt(x * x + y * y + z * z)
    theta = math.acos(z / r)
    phi = math.atan(y / x)
    return r, theta, phi


def deg2rad(x):
    return x / 180 * math.pi


def rad2deg(x):
    return x * 180 / math.pi


if __name__ == '__main__':
    r = 1
    print(F"   theta  phi       x       y      z")
    print(F"--------  ---  ------ ------- ------")  
    for theta in range(0, 360, 10):
        for phi in range(-90, 90, 10):
            x, y, z = sphere2cart(r, deg2rad(theta), deg2rad(phi))
            print(F"{theta:8} {phi:4}: {x:6.3f} {y:6.3f} {z:6.3f}")
