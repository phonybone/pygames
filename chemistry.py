"""
Some simulated biology-inspired, abstract reactions.

Types of molecules are represented by integers.  Reactiveness
with other types of molecues is represented by the amount of
bit-wise complimentry places.  For example,

1001101011 and
 110010100

binds with strength 7 because there are seven complimentry
bits, in order.

Consider:

111100001111 and
    1111

They bind at the illustrated position with strength 4, but
we have to find the location of maximum binding; we can't
just use ^ (xor) and call it good.



A catalyst has the power to change the nature of a molecule by
flipping/altering bits. (NYI)
"""

from functools import partial


def itob(i, width=None):
    """ replacement for bin(i) that omits leading "0b" and also does padding if asked """
    b = "{0:b}".format(i)
    if width:
        if width > 0:
            b = b.rjust(width, '0')
        else:
            b = b.ljust(-width, '0')
    return b


btoi = partial(int, base=2)


def bind_strength(a, b):
    ''' return the "bind strength" of two molecules as represented by the number of complimentry bits.  '''
    return bin(a ^ b).count('1')  # this is fastest until we install gmpy2


def bit_size(a):
    """
    return the number of bits
    required to represent a.  Eg, bit_size(1) is 1, bit_size(1001) is 4.

    Assumes a >= 0
    """
    size = 0
    while a > 0:
        a >>= 1
        size += 1
    return size


def mask(a):
    ''' return a mask of size a, where a mask is a string of set bits '''
    return (1 << bit_size(a)) - 1


def max_bind(a, b):
    """
    Return strength and position (tuple) of max bind between two molecules.
    Position is the left-most bit position where the max-binding occurs.
    """
    bigger = max(a, b)
    smaller = min(a, b)
    max_bind = 0
    pos = 0                     # number of bits to left-shift smaller
    p = 0

    m = mask(smaller)
    while bigger >= smaller:
        window = bigger & m
        bs = bind_strength(window, smaller)
        if bs > max_bind:
            max_bind = bs
            pos = p
        bigger >>= 1
        p += 1
    return max_bind, pos


def rand_bitstr(length):
    """ Return a random bitstring of length=length """
    return "".join(['1' if r.randint(0, 1) == 1 else '0' for _ in range(length)])


if __name__ == '__main__':
    import random as r
    r.seed(543)
    from timeit import timeit

    def rand_polynomial(order):
        """ Return a function that is a polynomial of order=order with random coefficients """
        coeffs = [r.randint(0, 10) for _ in range(order)]
        def f(x):
            return sum([coeffs[n] * (x**n) for n in range(order)])
        f.s = " + ".join([f"{coeffs[n]}*x^{n}" for n in range(order)])  # string representation
        return f

    def test_rand_poly():
        rows = []
        N = 20
        X = 20
        rows.append("  N  | " + "".join([" {:4} ".format(n) for n in range(N)]))
        rows.append("-" * len(rows[0]))
        for n in range(N):
            f = rand_polynomial(n)
            bss = [bit_size(f(x)) for x in range(X)]
            row = " {:2}  | ".format(n) + "".join([" {:4} ".format(bs) for bs in bss])
            rows.append(row)
        print("\n".join(rows))
    test_rand_poly()

    def test_max_bind():
        TESTCASES = (
            ('111100001111', '1111', 4, 4),
        )
        for case in TESTCASES:
            a, b, exp_bind, exp_pos = case
            i, j = btoi(a), btoi(b)
            print(f"a={a}, b={b}")
            bind, pos = max_bind(i, j)
            assert bind == exp_bind, f"bind={bind}, exp_bind={exp_bind}"
            assert pos == exp_pos, f"pos={pos}, exp_pos={exp_pos}"

            # attempt to see how fast is max_bind:
            n = 100_000
            globals()['i'] = i
            globals()['j'] = j
            t = timeit('max_bind(i, j)', number=n, setup='from __main__ import max_bind, i, j')
            print(f"timeit: n={n}, t={t}")

    # test_max_bind()
