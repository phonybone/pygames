'''
Basic 2D graphics transforms using numpy
'''
from math import sin, cos
import numpy as np

def rot2d(theta):
    this = np.zeros(9).reshape(3,3)
    this[0][0] = cos(theta)
    this[0][1] = -sin(theta)
    this[1][0] = sin(theta)
    this[1][1] = cos(theta)
    this[2][2] = 1.0
    return this


def transform2d(x0, x1, x2,
                y0, y1, y2,
                z0, z1, z2):
    return np.array(((x0, x1, x2), (y0, y1, y2), (z0, z1, z2)), dtype=float)


def scale2d(dx, dy):
    return np.array(((dx, 0, 0), (0, dy, 0), (0, 0, 1)), dtype=float)


def translate2d(dx, dy):
    return np.array(((1, 0, 0), (0, 1, 0), (dx, dy, 1)), dtype=float)


def world2grid(vpX, vpY, x0, x1, y0, y1):
    '''
    return a transform that maps world coords to a viewport.
    vpX, vpY are viewport dimensions.
    x0, x1, y0, y1 are boundaries of world rectangle.
    '''
    # translate world coords to origin:
    w2o = translate2d(-x0, -y0)

    # scale world coords to unit square:
    width = x1 - x0
    height = y1 - y0
    w2u = scale2d(1.0/width, 1.0/height)

    # invert y axis:
    inv = transform2d(1,  0, 0, 
                      0, -1, 0,
                      0,  1, 1)

    # scale back up to grid size:
    u2g = scale2d(vpX, vpY)

    # put it all together:
    t = w2o.dot(w2u).dot(inv).dot(u2g)
    return t


def grid2world(vpX, vpY, x0, x1, y0, y1):
    ''' inverse transform of world2grid '''
    g2u = scale2d(1/float(vpX), 1/float(vpY))
    inv = transform2d(1,  0, 0, 
                      0, -1, 0, 
                      0,  1, 1)
    width = x1-x0
    height = y1-y0
    u2w = scale2d(width, height)  # unit -> world
    print('u2w:\n{}'.format(u2w))
    o2w = translate2d(x0, y0)   # origin -> world
    print('o2w:\n{}'.format(o2w))
    return g2u.dot(inv).dot(u2w).dot(o2w)


def grid2unit(vpx, vpy):
    ''' assuming grid dimensions vpx X vpy (0-based), and "inverted" y axis,
    return a transform of grid back to the unit square
    '''
    g2u = scale2d(1/float(vpx), 1/float(vpy))
    inv = transform2d(1,  0, 0, 
                      0, -1, 0, 
                      0,  1, 1)
    return g2u.dot(inv)


__p = np.array((0, 0, 1), dtype=float)


def tr(t, x, y):
    ''' apply a transform to a point '''
    __p[0] = x
    __p[1] = y
    tx, ty, _ = __p.dot(t)
    return tx, ty
