# coding: utf-8
'''
Create 2D grids
'''

import numpy as np


def grid_idxs(x, y):
    ''' generate all grid indexes [i,j] for 0<=i<x, 0<=j<y '''
    for i in range(x):
        for j in range(y):
            yield i, j


class Grid(object):
    '''
    Wrapper around a 2-D numpy array.
    '''
    def __init__(self, R, C, dtype, cons, *cons_args):
        '''
        Return a 2D grid, R X C in size.

        If a constructor is passed, init each cell
        with the constructor, passing grid coords to
        constructor; otherwise None.
        '''
        self.w = int(R)
        self.h = int(C)
        if cons is None:
            def cons(i, j):
                return None
        # self.g = [[ cons(i,j, *cons_args) for i in range(h)] for j in range(w)]
        # datatype = np.dtype([('x', np.float64, (1,)), ('y', np.float64, (1,))])
        data = [cons(i, j, *cons_args) for i, j in grid_idxs(R, C)]
        self.g = np.array(data, dtype=dtype).reshape((R, C))

    def __getattr__(self, attr):
        if hasattr(self.g, attr):
            return getattr(self.g, attr)
        raise AttributeError(attr)

    def __getitem__(self, key):
        return self.g[key]

    @property
    def width(self):
        return self.w

    @property
    def height(self):
        return self.h

    @property
    def size(self):
        return self.width, self.height

    def at(self, x, y):
        return self.g[x][y]

    def put(self, x, y, v):
        self.g[x][y] = v

########################################################################


class GridOfList(Grid):
    '''
    Each grid element is a list
    '''
    def __init__(self, R, C):
        super(GridOfList, self).__init__(R, C, np.dtype(object), GridOfList._list_wrapper)

    class _list_wrapper:
        def __init__(self, i, j):
            self.l = list()

        def __str__(self):
            return "[" + ", ".join(map(str, self.l)) + "]"

        def __getattr__(self, attr):
            return getattr(self.l, attr)

        def __len__(self):
            return len(self.l)

########################################################################


def new_grid(w, h, dtype, cons, *cons_args):
    ''' 
    Return a 2D grid, r X c in size.
    If a constructor is passed, init each cell with the constructor, otherwise None
    '''
    w = int(w)
    h = int(h)
    if cons is None:
        def cons(i, j):
            return None
    # return [[ cons(i,j, *cons_args) for i in range(h)] for j in range(w)]

    return Grid(w, h, dtype, cons, *cons_args)


if __name__ == '__main__':
    g = GridOfList(5, 5)
    l_11 = g.at(1, 1)
    print(f"l_11: {l_11} (t={type(l_11)})")
    l_11.append('hi')
    l_11.append('there')
    l_11.append('you')
    l_11.append('cutie')
    print(f"l_11: {l_11}")
