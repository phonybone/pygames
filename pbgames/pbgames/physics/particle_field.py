'''
Compute the pairwise forces on a particle cloud, and update each
particle accordingly.

Optimize by clustering particles in to a grid (or whatever),
computing the local "center of force" for each cell
'''

import math
from particle import Particle2D
from grid import GridOfList


class Cluster:
    def __init__(self, particles, nrow=None, ncol=None):
        if nrow is None or ncol is None:
            nrow, ncol = self._auto_dimensions(particles)
        self.nrow = nrow
        self.ncol = ncol
        self.grid = GridOfList(nrow, ncol)
        self._partition(particles)

    @staticmethod
    def _auto_dimensions(particles):
        ''' determine dimensions given the number of particles '''
        n1 = math.pow(len(particles), 1.0 / 3)  # first take cube root
        n2 = math.pow(2, math.ceil(math.log(n1, 2)))  # then bump up to next power of 2
        n3 = math.pow(2, math.floor(math.log(n1, 2)))  # then bump down to previous power of 2
        n = int(n3 if abs(n1 - n2) > abs(n1 - n3) else n2)  # then take the closest one
        nrow = ncol = n
        return nrow, ncol

    # find range of particle positions:
    @staticmethod
    def _ranges(particles):
        ''' find the bounding box of all particles  '''
        p0 = particles[0]
        minx, miny, maxx, maxy = p0.x, p0.y, p0.x, p0.y
        for p in particles:
            if p.x < minx:
                minx = p.x
            if p.x > maxx:
                maxx = p.x
            if p.y < miny:
                miny = p.y
            if p.y > maxy:
                maxy = p.y
        spanx = maxx - minx
        spany = maxy - miny
        return minx, miny, maxx, maxy, spanx, spany

    def _partition(self, particles):
        ''' assign each particle to a grid cell '''
        minx, miny, maxx, maxy, spanx, spany = self._ranges(particles)
        dx = 1e-8 * spanx
        dy = 1e-8 * spany
        for p in particles:
            r = int((p.x - minx - dx) / spanx * self.nrow)
            c = int((p.y - miny - dy) / spany * self.ncol)
            self.grid[r][c].append(p)

    def stats(self):
        ''' compute mean, stddev, min, max of number of particles per cell '''
        x = 0                   # tmp sum
        xx = 0                  # tmp sum^2
        miny = maxy = len(self.grid[0][0])
        for r in range(self.nrow):
            for c in range(self.ncol):
                y = len(self.grid[r][c])
                x += y
                xx += y * y
                if y > maxy:
                    maxy = y
                if y < miny:
                    miny = y
        N = self.nrow * self.ncol
        avg = float(x) / N
        stdev = math.sqrt(xx / (N-1) - avg*avg)
        return miny, maxy, avg, stdev

        # print 'x, xx, N', x, xx, N
        # print 'miny, maxy', miny, maxy
        # print 'avg',avg
        # print 'std',stdev


def consolidate(grid):
    ''' calculate the effective mass of all particles in the grid. '''
    r = len(grid)
    c = len(grid[0])
    print('r, c', r, c)
    g2 = new_xgrid(r, c)
    for i in range(r):
        for j in range(c):
            X = 0
            Y = 0
            M = 0
            for p in grid[i][j]:
                X += p.x
                Y += p.y
                M += p.mass
            N = float(len(grid[i][j]))
            P = Particle2D(X/N, Y/N, 0, 0, M)
            # print 'g2[{}][{}]: {} {}'.format(i, j, P, N)
            g2[i][j] = P
    return g2


if __name__ == '__main__':
    particles = [Particle2D.random() for _ in range(10000)]
    cluster = Cluster(particles)
    print(f'stats(cluster): {cluster.stats()}')
