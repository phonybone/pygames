from pbgames.utils import magnitude

G = 6.673e-11
FORCEMULT = 1e3

def gravity(p1, p2):
    '''
    2D gravitation force between two particles, expressed as the force on p1 (tuple). 
    for force on p2, invert.

    @parms
    p1, p2: both things that have .pos attrs represented as np.arrays and mass attrs
    return an np.array representing the force (pointing from p1 to p2)
    '''
    mag = G*p1.mass*p2.mass
    dp = p2.pos - p1.pos
    dist = magnitude(dp)
    dist2 = dist * dist
    return (dp * mag) / dist2   # * FORCEMULT

if __name__ == '__main__':
    import numpy as np
    from collections import namedtuple
    Particle = namedtuple('Particle', ['pos', 'mass'])
    p1 = Particle(pos=np.array([1.0, 0.0]), mass=1.0)
    for i in range(10):
        p2 = Particle(pos=np.array([0.0, float(i)]), mass=1.0)
        Fg = gravity(p1, p2)
        print(F"Fg: {Fg}")
