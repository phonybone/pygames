#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''
Create a 2D grid of forces that can be applied to particles.

Author: Victor Cassen
'''

import sys
import os
import pygame
import random
import numpy as np

# add the parent directory to sys.path in order to get to pygames:
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), os.path.pardir)))
from gamebase import Game
from grid import new_grid
from particle import Particle2D
from transform import world2grid, grid2world, tr, translate2d
from utils2d import normalize, scale
import colors 


SCREEN_WIDTH =    640
SCREEN_HEIGHT =   640

GRID_WIDTH =        10           # 
GRID_HEIGHT =       10
print('GRID WxH: {}x{}'.format(GRID_WIDTH, GRID_HEIGHT))

WORLD_LEFT =      -10
WORLD_BOTTOM =    -10
WORLD_TOP =        10
WORLD_RIGHT =      10
WORLD_WIDTH =      WORLD_RIGHT - WORLD_LEFT
WORLD_HEIGHT =     WORLD_TOP - WORLD_BOTTOM
print('WORLD WxH: {}x{}'.format(WORLD_WIDTH, WORLD_HEIGHT))

MAX_DX = 10
MAX_DY = 10
#N = 80                          # scaling factor, of sortas

class FFGame(Game):
    def __init__(self):
        super(FFGame, self).__init__((SCREEN_WIDTH, SCREEN_HEIGHT), colors.black)
        t = self.set_world(-WORLD_WIDTH/2, WORLD_WIDTH/2, 
                           -WORLD_HEIGHT/2, WORLD_HEIGHT/2)
        print(t)
        self.g2w = grid2world(GRID_WIDTH-1, GRID_HEIGHT-1, 
                              -WORLD_WIDTH/2, WORLD_WIDTH/2,
                              -WORLD_HEIGHT/2, WORLD_HEIGHT/2)
        print('g2w:\n{}'.format(self.g2w))
        self.w2g = world2grid(GRID_WIDTH, GRID_HEIGHT, 
                              -WORLD_WIDTH/2, WORLD_WIDTH/2,
                              -WORLD_HEIGHT/2, WORLD_HEIGHT/2)
        
        dtype = np.dtype([('x', np.float64, (1,)), ('y', np.float64, (1,))])
        self.grid = new_grid(GRID_WIDTH, GRID_HEIGHT, dtype, self.init_cell)

        self.p0 = Particle2D(random.random()*WORLD_WIDTH - WORLD_WIDTH/2, 
                             random.random()*WORLD_HEIGHT - WORLD_HEIGHT/2, 
                             random.random()*MAX_DX, random.random()*MAX_DY,
                             100)

    def init_cell(self, gx, gy):
        sink_point = 3, -3
        sink = translate2d(*sink_point)
        x, y = tr(sink.dot(self.g2w), gx, gy)

        fx, fy = scale(3, *normalize(-x, -y))
        # fx = -y
        # fy = x
        # print('gx,gy={},{}; x,y={}, {}; f={}, {}'.format(gx, gy, x, y, fx, fy))
        return fx, fy 

    def init_game(self):
        pass

    def update(self):
        self.draw_ff()
        self.drawp0()
        return True

    def draw_ff(self):
        ''' draw the force field '''
        for gx in range(GRID_WIDTH):
            for gy in range(GRID_HEIGHT):
                f = self.grid.at(gx, gy)
                wp0 = tr(self.g2w, gx, gy)   # 
                f = (wp0[0]+f[0]/5, wp0[1]+f[1]/5) # force vector
                # print('ff wp0: {}, f: {}'.format(wp0, f))
                sp0 = self.w2s(*wp0) # get screen coords for vector
                sp1 = self.w2s(*f)
                pygame.draw.line(self.screen, colors.red, sp0, sp1, 1)
                pygame.draw.circle(self.screen, colors.white, sp0, 2, 1)

    def drawp0(self):
        ''' draw the moving dot on the screen '''
        screen = self.screen
        p0 = self.p0
        box = [-WORLD_WIDTH/2, -WORLD_HEIGHT/2, 
               WORLD_WIDTH/2, WORLD_HEIGHT/2]
        x, y = int(p0.x), int(p0.y)
        gx, gy = map(int, tr(self.w2g, x, y))
        f = self.grid.at(gx, gy)
        p0.push(*f)
        p0.move()
        p0.bounce(*box)
        p0.friction(0.1)
        sx, sy = self.w2s(p0.x, p0.y)
        # print('p0: {}; f: {}'.format(p0, f))
        pygame.draw.circle(screen, colors.white, (sx, sy), 10, 4)



def main(opts):
    game = FFGame()
    game.loop()
    return 0


def vortex(H, W, mag):
    ''' return a grid of circular forces '''
    center = (H/2, W/2)
    def f(r, c):
        pass

def getopts():
    import argparse
    parser = argparse.ArgumentParser(description='')
    parser.add_argument('--some-arg', default='some arg')
    parser.add_argument('-v', action='store_true', help='verbose')
    parser.add_argument('-d', action='store_true', help='debugging flag')

    opts=parser.parse_args()
    if opts.d:
        print(opts)
    return opts

def ppjson(o):
    import json
    return json.dumps(o, indent=2)


if __name__=='__main__':
    opts = getopts()
    try:
        rc=main(opts)
        if rc is None:
            rc=0
        sys.exit(rc)
    except Exception as e:
        if opts.d:
            import traceback
            traceback.print_exc()
        else:
            print('error: {} {}'.format(type(e), e))
        sys.exit(1)
