'''
grid of interpolated values.


'''

import math, random
#random.seed(0)
import numpy as np
import grid 


def w2g(grid, x, y):
    '''
    calculate the upper right index of the grid corresponding to x,y
    assume x,y inside unit square with origin in lower left
    '''
    gw, gh = grid.size
    i = int(math.floor(gw * x))
    j = int(math.floor(gh * y))
    return i,j

def interpolate(x0, x1, x):
    '''
    return linear interpolation between x0 and x1
    '''
    dx = x1 - x0
    return x0 + x*dx

if __name__ == '__main__':
    N = 4
    R = N+1
    C = N+1
    
    # create grid:
    def _init_cell(i, j):
        return random.random() * 10
    dtype=float
    g = grid.Grid(R, C, dtype, _init_cell)
    print g.g
    print g.g[2][0]             # r,c
    print '{} X {}'.format(g.width, g.height)
    print

    # pick a random point in each cell:
    for i, j in grid.grid_idxs(N, N):
        x, y = i + random.random(), j + random.random()
        print 'x, y: ({:.2f}, {:.2f})'.format(x,y)
        r, c = w2g(g, x/R, y/C)
        print 'w2g({:.2f}, {:.2f}): [{}][{}]'.format(x,y,r,c)
        print 'g[{}][{}]: {}'.format(r, c, g.g[r][c])
        print

        # get "box" of values around x, y:
        box = np.array((g.g[r][c],
                        g.g[r][c+1],
                        g.g[r+1][c],
                        g.g[r+1][c+1])).reshape(2,2)
        print box
        print

        # find partial x,y values (proprotion)
        px = x-r
        py = y-c
        print 'px, py: {:.2f}, {:.2f}'.format(px, py)

        # find enpoints of c->c+1 lines:
        x0 = interpolate(g.g[r][c],   g.g[r][c+1],   px) # |    left
        x1 = interpolate(g.g[r+1][c], g.g[r+1][c+1], px) #  |   right
        print 'x0, x1: {:.2f}, {:.2f}'.format(x0, x1)

        # find endpoints of r,r+1 line:                  
        y0 = interpolate(g.g[r][c],   g.g[r+1][c],   py) # -    top
        y1 = interpolate(g.g[r][c+1], g.g[r+1][c+1], py) # _    bottom
        print 'y0, y1: {:.2f}, {:.2f}'.format(y0, y1)

        z0 = interpolate(x0, x1, py)
        z1 = interpolate(y0, y1, px)
        avg = (z0 + z1) / 2
        print 'z0, z1, avg: {:.2f}, {:.2f}, {:.2f}'.format(z0, z1, avg)
        print '-' * 36, '\n'
