
def almost_equal(x,y,e=0.000000001):
    return abs(x-y) < e

def almost_zero(x,e=0.000000001):
    return abs(x) < e
