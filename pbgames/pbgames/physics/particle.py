# coding: utf-8
'''
Model a 2D particle that has mass, momentum, and responds to forces.
'''
from random import random
import numpy as np
import pbgames.utils as u

class Particle2D:
    def __init__(self, x, y, dx=0.0, dy=0.0, mass=1.0):
        self.pos = np.array([float(x), float(y)])
        self.vel = np.array([float(dx), float(dy)])
        self.mass = float(mass)

    # @property
    # def speed(self):
    #     ''' speed is the magnitude of velocity (scalar) '''
    #     return u.magnitude(self.vel)

    @property
    def momentum(self):
        return self.mass * self.vel

    def __str__(self):
        return '({:.2f}, {:.2f}), v=({:.2f}, {:.2f}), m={:.2f}'.format(
            self.pos[0], self.pos[1], self.vel[0], self.vel[1], self.mass
        )

    ########################################################################

    @classmethod
    def random(cls):
        return Particle2D(random(), random(), random(), random(), random())

    ########################################################################

    def move(self, C=1.0):
        ''' apply velocity to position '''
        self.pos = self.pos + (self.vel * C)

    def friction(self, CoF):
        self.vel = self.vel * CoF

    def accel(self, dv):
        ''' apply a repulsive force '''
        self.vel = self.vel + dv

if __name__ == '__main__':
    p = Particle2D(0, 0, 1, 1, 1)
    print(p)
    print('velocity', p.vel, u.magnitude(p.vel))
    print('momentum', p.momentum, u.magnitude(p.momentum))

    print(p)
    print('velocity', p.vel, u.magnitude(p.vel))
    print('momentum', p.momentum, u.magnitude(p.momentum))

    # p2 = Particle2D(3, 3, 0, 0, 1)
    # print('distance:  {} ({})'.format(p.dist(p2), math.pow(p.dist(p2), 2)))
    # print('distance2: {} ({})'.format(p.dist2(p2), math.sqrt(p.dist2(p2))))
