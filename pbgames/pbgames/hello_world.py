'''
Do the bouncing ball game
'''
from pbgame.gamebase import Game
from ball import Ball
from graph_obj import graph_obj
import colors

class BBGame(Game):
    def __init__(self, *args, **kwargs):
        super(BBGame, self).__init__(*args, **kwargs)
        self.movables = []
        self.drawables = []
        self.load_ball()
        self.load_graph()

    def load_ball(self):
        x = self.screen.get_width()/2
        y = self.screen.get_height()/2
        ball = Ball(self, x, y, 5, -12)
        self.movables.append(ball)
        self.drawables.append(ball)

    def load_graph(self):
        self.drawables.append(graph_obj)

    def render_frame(self, t, screen):
        for obj in self.movables:
            obj.move(t)
        for obj in self.drawables:
            obj.draw(screen)

def main():
    game = BBGame(1000, 800, colors.black)
    game.start()
    game.loop()

if __name__ == '__main__':
    main()
