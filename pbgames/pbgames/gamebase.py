'''
Base class for pygames
'''
import sys
import os
import signal
import time
import threading as th
import pygame
import numpy as np

from pbgames.transform import world2grid
from pbgames.colors import colors

def handler(args):
    ''' global thread exception handler '''
    print(F"thread name={args.thread.name}: caught {args.exc_type}: {args.exc_value}",
          file=sys.stderr)
    if args.exc_traceback is not None:
        import traceback as tb
        tb.print_exc()
    os.kill(os.getpid(), signal.SIGINT)  # sys.exit only kills the thread
th.excepthook = handler

class Game(object):
    ''' A Pygame '''
    def __init__(self,
                 screen_w, screen_h,
                 bottom, left, top, right,
                 background='black', fps=24, speedup=1.0):
        '''
        Game container class.  Providers game event loop.
        screen_w, screen_h: screen width, height
        background: a color from pbgames.colors, or a three-tuple
        fps: frames per second
        '''
        self.width, self.height = int(screen_w), int(screen_h)
        self.screen = pygame.display.set_mode((self.width, self.height))

        self.bottom = float(bottom)
        self.left = float(left)
        self.top = float(top)
        self.right = float(right)
        self.w_width = self.right - self.left
        self.w_height = self.top - self.bottom
        self.set_world(self.left, self.right, self.bottom, self.top)
        self.background = colors[background]

        self.t = 0              # global time
        self.fps = int(fps)
        self.zzz = 1.0/self.fps        # sleep time
        self.t_speedup = float(speedup)  # gametime scale factor
        self.dt = self.zzz * self.t_speedup
        print(F"dt={self.dt}={self.zzz}*{self.t_speedup}")

        self.start_drawing = th.Event()
        self.flip_ready = th.Event()
        self.game_over = th.Event()

        pygame.init()

    @classmethod
    def from_config(cls, config):
        args = []
        try:
            args.append(config.screen_x)
            args.append(config.screen_y)
            args.append(config.bottom)  # order of these four is critical
            args.append(config.left)
            args.append(config.top)
            args.append(config.right)
        except AttributeError as e:
            raise RuntimeError(F"missing config value: {e}")

        keys = ('background', 'fps', 'speedup')
        kwargs = {}
        for key in keys:
            try:
                kwargs[key] = getattr(config, key)
            except AttributeError:
                pass
        return cls(*args, **kwargs)

    def event_report(self):
        ''' return a string describing state of threading Events '''
        SD = F"{self.start_drawing.is_set()}"
        FR = F"{self.flip_ready.is_set()}"
        return F"SD={SD} FR={FR}"

    def flip_thread(self):
        ''' flip the display buffer; set threading Events '''
        while True:
            self.flip_ready.wait()
            pygame.display.flip()
            self.screen.fill(colors['black'])
            self.flip_ready.clear()

    def sleep_thread(self):
        ''' Coordinate threading events '''
        while True:
            self.start_drawing.set()
            self.flip_ready.clear()
            time.sleep(self.zzz)
            self.t += self.dt

    def start(self):
        ''' create and start threads '''
        self.flipper = th.Thread(name='flipper', target=self.flip_thread, daemon=True)
        self.sleeper = th.Thread(name='sleeper', target=self.sleep_thread, daemon=True)

        self.flipper.start()
        self.sleeper.start()

    def loop(self):
        ''' wrapper for main game loop '''
        rc = 0
        try:
            self._loop()
        except Exception as e:
            print(F"caught {type(e)}: {e}", file=sys.stderr)
            import traceback as tb
            tb.print_exc()
            rc = 1
        finally:
            pygame.quit()
            sys.exit(rc)

    def render_frame(self):
        raise NotImplementedError()

    def _loop(self):
        '''
        Main event loop:
          - consume/process all pygame.events
          - clear screen
          - call self.update() to draw new screen
          - call pygame.display.flip()
        '''
        while True:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    print("Quitting!")
                    return
            self.start_drawing.wait()
            self.render_frame()
            self.start_drawing.clear()
            self.flip_ready.set()

    def set_world(self, x0, x1, y0, y1):
        ''' set world coordinates '''
        sx, sy = self.screen.get_size()
        self.w2g = world2grid(sx, sy, x0, x1, y0, y1)
        return self.w2g

    # __P is just used as a temp for transforming points
    __p = np.array((0, 0, 1), dtype=float)
    eps = 1e-6

    def w2s(self, wx, wy):
        ''' translate world coords to the current screen '''
        self.__p[0] = float(wx)-self.eps
        self.__p[1] = float(wy)-self.eps
        sx, sy, _ = self.__p.dot(self.w2g)
        return int(sx), int(sy)
