from functools import wraps
import math as m
import numpy as np
from time import time

TWO_pi = m.pi * 2
HALF_PI = m.pi / 2.0

def normalized(a: np.array, axis=-1, order=2):
    '''
    Normalize a np vector
    '''
    l2 = np.atleast_1d(np.linalg.norm(a, order, axis))
    l2[l2 == 0] = 1
    # return a / np.expand_dims(l2, axis)
    return a / l2

def distance(a, b, _norm=np.linalg.norm):
    return _norm(a-b)
    # see https://stackoverflow.com/questions/1401712/how-can-the-euclidean-distance-be-calculated-with-numpy, answer 

def magnitude(a, _norm=np.linalg.norm):
    return _norm(a)

def to_radians(x, y):
    if x == 0:
        return HALF_PI if y > 0 else -HALF_PI
    if x>0 and y>=0:
        return m.atan(y/x)
    if x<0 and y>=0:
        return m.atan(y/x)+m.pi
    if x<0 and y<=0:
        return m.atan(y/x)+m.pi
    if x>0 and y<=0:
        return m.atan(y/x)+m.pi+m.pi


def to_polar(x, y):
    r = m.sqrt(x*x + y*y)
    theta = to_radians(x, y)
    return r, theta

def from_polar(r, theta):
    return r * m.cos(theta), r * m.sin(theta)

def dot2(a, b):
    ''' dot product for 2D '''
    return a[0]*b[0] + a[1]*b[1]

def dot3(a, b):
    ''' dot product for 3D '''
    return a[0]*b[0] + a[1]*b[1] + a[2]*b[2]

def time_this(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        t0 = time()
        ret = func(*args, **kwargs)
        duration = time() - t0
        print(F"{func.__name__}: duration={duration:.3f} ({1.0/duration:.1f} fps)")
        return ret
    return wrapper

def time_this_method(func):
    @wraps(func)
    def wrapper(self, *args, **kwargs):
        t0 = time()
        ret = func(*args, **kwargs)
        duration = time.time() - t0
        print(F"{func.__name__}: duration={duration:.3f} ({1.0/duration:.1f} fps)")
        return ret
    return wrapper

if __name__ == '__main__':
    def near(a, b, eps=1e-6):
        return abs(a-b) < eps

    dtheta = 3
    for i in range(int(360/dtheta)):
        theta = m.radians(i * dtheta)
        x = m.cos(theta)
        y = m.sin(theta)
        rad = to_radians(x,y)
        deg = m.degrees(rad)
        print(F"{i*dtheta:4} ({x:4.2f}, {y:4.2f}): rad={rad:.2f} deg={deg:.0f}")

    # X, Y = -10.0, -10.0
    # h = 0.01
    # x, y = X, Y
    # eps = 1e-6
    # while True:
    #     print(F"x={x}, y={y}")
    #     r, theta = to_polar(x, y)
    #     print(F"r={r}, theta={theta}")
    #     nx, ny = from_polar(r, theta)
    #     assert near(x, nx, eps), F"x={x}, nx={nx}, x-nx={abs(x-nx)}, eps={eps}"
    #     assert near(y, ny, eps), F"y={y}, ny={ny}, y-ny={abs(y-ny)}, eps={eps}"
    #     x += h
    #     if x > -X:
    #         x = X
    #         y += h
    #         if y > -Y:
    #             break
