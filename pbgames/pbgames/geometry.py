import math

def in_box(x, y, x0, y0, x1, y1):
    ''' is point (x,y) within box bounded by x0, y0, x1, and y1? '''
    return x >= x0\
        and x <= x1\
        and y >= y0\
        and y <= y1

def in_circle(x, y, cx, cy, r):
    x -= cx
    y -= cy
    return x*x + y*y <= r*r
    
