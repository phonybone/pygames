import numpy as np

def normalized(a: np.array, axis=-1, order=2):
    '''
    Normalize a np vector
    '''
    l2 = np.atleast_1d(np.linalg.norm(a, order, axis))
    l2[l2 == 0] = 1
    # return a / np.expand_dims(l2, axis)
    return a / l2
