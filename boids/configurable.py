from pathlib import Path
from pbutils.nested_attrs import set_nested_attr, get_nested_attrs
from pbutils.nested_attrs import config_from_file, config_from_lines


class Configurable:
    '''
    Class decorator: assigns class variables from config object.
    config object can be file or stream.
    '''
    def __init__(self, config):
        if isinstance(config, str):
            path = Path(config)
            if path.exists():
                config = config_from_file(path)
            else:
                config = config_from_lines(path.split('\n'))
        self.config = config

    def __call__(self, cls):
        conf_section = getattr(self.config, cls.__name__)
        if conf_section is None:
            raise ValueError(F"No section '{cls.__name__}' in config")
        for path, val in get_nested_attrs(conf_section):
            set_nested_attr(cls, path, val)
        return cls


if __name__ == '__main__':
    class Config:
        pass
    
    config = Config()
    config.Fred = Config()
    config.Fred.key1 = 'value1'
    config.Fred.key2 = 'value2'
    config.Fred.key3 = 'value3'

    @Configurable(config)
    class Fred:
        pass

    print(F"Fred.key1={Fred.key1}")
    print(F"Fred.key2={Fred.key2}")
    print(F"Fred.key3={Fred.key3}")
    
    @Configurable('main.ini')
    class Birds:
        pass

    for path, value in get_nested_attrs(Birds):
        print(F"Birds.{path} = {value}")
