'''
'''
from boid import new_boid

class Flock:
    '''
    Simple container class for Boids
    '''
    def __init__(self):
        self.boids = []

    def load(self, n_boids, config):
        ''' '''
        bottom = float(config.bottom)
        left = float(config.right)
        top = float(config.top)
        right = float(config.left)
        for _ in range(int(n_boids)):
            self.boids.append(new_boid(bottom, left, top, right))
        return self

    def move(self, game):
        '''
        compute all forces on each boid,
        then move each according to the force.
        '''
        for boid in self.boids:
            boid.move(self, game)

    def draw(self, game):
        for boid in self.boids:
            boid.draw(game)

    def add_boid(self, boid):
        self.boids.append(boid)

    def add_boids(self, boids):
        self.boids.extend(boids)

if __name__ == '__main__':
    flock = Flock()
    X, Y = 100, 100
    for _ in range(10):
        flock.add_boid(new_boid(X, Y))
    flock.move(0)
    # flock.draw(None)
    
