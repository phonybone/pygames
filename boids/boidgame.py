"""
BoidGame
"""
from pbgames.gamebase import Game
from pbgames.utils import time_this

from flock import Flock

class BoidGame(Game):
    def __init__(self, *args, **kwargs):
        super(BoidGame, self).__init__(*args, **kwargs)

    @classmethod
    def from_config(cls, config):
        game = super(BoidGame, cls).from_config(config)
        game._init(config)
        return game

    def _init(self, config):
        self.movables = []
        self.drawables = []
        self.load_flock(config)

    def load_flock(self, config):
        '''
        '''
        n_boids = config.Boid.n_boids
        flock = Flock().load(n_boids, config)
        self.movables.append(flock)
        self.drawables.append(flock)

    @time_this
    def render_frame(self):
        self.screen.fill(self.background)
        for obj in self.movables:
            obj.move(self)
        for obj in self.drawables:
            obj.draw(self)
