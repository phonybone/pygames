import random
import numpy as np
import math
import pygame as pg

from pbgames.colors import colors, random_color
from pbgames.transform import rot2d, translate2d
from pbgames.utils import normalized, to_radians, magnitude

from configurable import Configurable
_X = 0
_Y = 1


@Configurable('main.ini')
class Boid:
    __id = 0
    maxforce = None
    maxspeed = None
    target_sep = None

    # for drawing:
    dpoints2 = (
        np.array((-15, 15)),
        np.array((-15, -15)),
        np.array((30, 0))
    )
    dpoints3 = (
        np.array((-15, 15, 1)),
        np.array((-15, -15, 1)),
        np.array((30, 0, 1))
    )

    def __init__(self, pos, vel, acc, color,
                 sep_weight=1.0, align_weight=1.0, cohesion_weight=1.0):
        self.id = self.__id
        Boid.__id += 1

        self.pos = pos
        self.vel = vel
        self.acc = acc
        self.color = color
        self.sep_weight = sep_weight
        self.align_weight = align_weight
        self.cohesion_weight = cohesion_weight

    def set_pos(self, x, y):
        self.pos = np.array([float(x), float(y), 1.0])

    def set_vel(self, x, y):
        self.vel = np.array([float(x), float(y), 1.0])

    def set_acc(self, x, y):
        self.acc = np.array([float(x), float(y), 1.0])

    @property
    def heading(self):
        ''' return heading in radians '''
        rad = to_radians(self.vel[_X], self.vel[_Y])
        if rad is None:
            breakpoint()
        return -rad

    def __str__(self):
        return F"id={self.id}: pos={self.pos}"

    def __repr__(self):
        return F"<Boid id={self.id} pos={self.pos} vel={self.vel} acc={self.acc} heading={self.heading}>"

    def __eq__(self, other):
        return self.id == other.id

    def __ne__(self, other):
        return not self == other

    def move(self, flock, game):
        ''' Calculate and apply acceleration. '''
        steer = self.flock(flock.boids)
        if any(x==float('nan') for x in steer):
            breakpoint()
        mag = magnitude(steer)
        if mag > self.maxforce:
            # print(F"boid[{self.id}]: capping steer")
            steer /= mag
        if mag is None:
            breakpoint()

        self.acc = steer
        self.vel += self.acc
        self.pos += self.vel
        self.wrap_pos(game)
        # print(repr(self))

    def distance(self, other):
        diff = self.pos - other.pos
        return np.linalg.norm(diff)

    def flock(self, boids):
        '''
        do all computing to determine forces on this boid.
        @params:
        @boids: list[Boid]

        returns an np.array with shape (1,2) (2D force vector)
        '''
        self.sep = self.separate(boids) * self.sep_weight
        # print(F"boid {self.id}: sep={self.sep}")
        self.ali = self.align(boids) * self.align_weight
        # print(F"boid {self.id}: ali={self.ali}")
        self.coh = self.cohesion(boids) * self.cohesion_weight
        # print(F"boid {self.id}: coh={self.coh}")
        force = self.sep + self.ali + self.coh
        # print(F"boid {self.id}: force={force}")
        if 'nan' in str(force):
            breakpoint()
        return force

    def separate(self, boids):
        '''
        check for nearby boids and steer away:
        @params:
        @boids: list[Boid]
        '''
        steer = np.array([0.0, 0.0])
        # steer.resize(1,3)
        count = 0
        for other in boids:
            if self == other:
                continue

            dist = self.distance(other)
            # print(F"boid[{self.id}] <-> boid[{other.id}]: dist={dist}")
            if dist == 0:
                dist = 0.001
            if dist < self.target_sep:
                diff = normalized(self.pos - other.pos) / dist
                steer = steer + diff
                # print(F"  diff={diff}, steer={steer}")
                count += 1

        if count == 0:
            return steer

        steer /= count
        if not all(x == 0 for x in steer):
            steer = normalized(steer)
            steer *= self.maxspeed
            steer -= self.vel
            steer = np.clip(steer, -self.maxforce, self.maxforce)
        return steer

    def align(self, boids):
        '''
        For every nearby boid in the system, calculate the average velocity.
        '''
        steer = np.array([0.0, 0.0])
        count = 0
        for other in boids:
            if self == other:
                continue
            dist = self.distance(other)
            if dist > self.neighbordist:
                steer = steer + other.vel
                count += 1

        if count == 0:
            # print("align: no close boids")
            return steer

        steer /= count
        steer = normalized(steer)
        steer *= self.maxspeed
        steer -= self.vel
        steer = np.clip(steer, -self.maxforce, self.maxforce)
        return steer

    def cohesion(self, boids):
        '''
        For the average position (i.e. center) of all nearby boids,
        calculate steering vector towards that position
        '''
        steer = np.array([0.0, 0.0])
        count = 0
        for other in boids:
            if self == other:
                continue
            dist = self.distance(other)
            if dist < self.neighbordist:
                steer = steer + other.pos  # Add position
                count += 1

        if count > 0:
            steer /= count
        return self.seek(steer)  # Steer towards the position

    def seek(self, target: np.array):
        '''
        A method that calculates and applies a steering force towards a target
        STEER = DESIRED MINUS VELOCITY
        '''
        # Scale to maximum speed
        desired = normalized(target - self.pos) * self.maxspeed

        # Steering = Desired minus Velocity
        steer = desired - self.vel        # or vel - desired?
        steer = np.clip(steer, -self.maxforce, self.maxforce, out=steer)
        return steer

    def draw(self, game):
        screen = game.screen
        rot = rot2d(self.heading)
        trnslt = translate2d(self.pos[_X], self.pos[_Y])
        trn = np.matmul(rot, trnslt)
        pts = [np.matmul(pt, trn) for pt in self.dpoints3]
        pts.append(pts[0])      # make a loop
        lastpt = pts[0]
        for pt in pts[1:]:
            start = (lastpt[_X], lastpt[_Y])
            start = game.w2s(*start)
            end = (pt[_X], pt[_Y])
            end = game.w2s(*end)
            # print(F"pg.draw.line(screen, color, {start}, {end}, width=1)")
            pg.draw.line(screen, self.color, start, end, width=1)
            lastpt = pt

        # self.draw_forces(game)

    def draw_forces(self, game):
        screen = game.screen
        pos_v = game.w2s(*self.pos)
        scale = 12
        coh_v = self.pos + self.coh * scale
        coh_v = game.w2s(*coh_v)
        pg.draw.line(screen, colors['red1'].as_tup(), pos_v, coh_v, width=1)

        ali_v = self.pos + self.coh * scale
        ali_v = game.w2s(*ali_v)
        pg.draw.line(screen, colors['green'].as_tup(), pos_v, ali_v, width=1)
        sep_v = self.pos + self.coh * scale
        sep_v = game.w2s(*sep_v)
        pg.draw.line(screen, colors['yellow1'].as_tup(), pos_v, sep_v, width=1)

    # def clip_pos(self, screen):
    #     maxx, maxy = screen.get_size()
    #     if self.pos[_X] < 0:
    #         self.pos[_X] = 0
    #     if self.pos[_X] > maxx:
    #         self.pos[_X] = maxx
    #     if self.pos[_Y] < 0:
    #         self.pos[_Y] = 0
    #     if self.pos[_Y] > maxy:
    #         self.pos[_Y] = maxy

    def wrap_pos(self, game):
        if self.pos[_X] < game.left:
            self.pos[_X] += game.w_width
        if self.pos[_X] > game.right:
            self.pos[_X] -= game.w_width
        if self.pos[_Y] < game.bottom:
            self.pos[_Y] += game.w_height
        if self.pos[_Y] > game.top:
            self.pos[_Y] -= game.w_height


def new_boid(b, l, t, r):
    '''
    Make a new boid.
    b, l, t, r: bounding box of world (expects floats)
    '''
    # position:
    width = r - l
    height = t - b
    x = random.random() * float(width) + l
    y = random.random() * float(height) + b
    pos = np.array([x, y])

    # velocity:
    angle = random.random() * 2 * math.pi
    dx, dy = math.sin(angle), math.cos(angle)
    vel = np.array([dx, dy])

    # acceleration
    acc = np.array([0, 0])

    color = random_color().as_tup()
    boid = Boid(pos, vel, acc, color)
    return boid

if __name__ == '__main__':
    # for key, value in Boid.__dict__.items():
    #     if not key.startswith('_'):
    #         print(F"Boid.{key} = {value} ({type(value)})")

    boid = new_boid(1000, 1000)
    print(repr(boid))
    boid.set_vel(0, 10)
    print(repr(boid))

    boid.draw(screen=None)
