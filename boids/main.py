#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''
Playing with boids
Code stolen from https://processing.org/examples/flocking.html
See also https://www.red3d.com/cwr/boids/

Author: Victor Cassen
vmc.swdev@gmail.com
'''
from boidgame import BoidGame
from pbutils.argparsers import parser_stub, wrap_main

def main(config):
    game = BoidGame.from_config(config)
    game.start()
    game.loop()

    return 0


def make_parser():
    parser = parser_stub(__doc__)
    parser.add_argument('-X', '--width', default=640, type=int, help='width of world')
    parser.add_argument('-Y', '--height', default=320, type=int, help='height of world')
    parser.add_argument('--n-boids', default=10, type=int, help='number of boids')
    parser.add_argument('arg', nargs='*')

    return parser

if __name__ == '__main__':
    parser = make_parser()
    wrap_main(main, parser)    
