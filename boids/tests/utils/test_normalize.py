import numpy as np
from utils import normalized

def test_normalized():
    a = np.array([3, 4])
    n = normalized(a)
    assert n[0][0] == 3/5
    assert n[0][1] == 4/5
