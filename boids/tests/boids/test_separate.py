import numpy as np

def test_separate(test_flock):
    sep = test_flock.boids[0].separate(test_flock.boids)
    print(F"sep: {sep}")

def test_separate_left(test_flock):
    print()
    test_flock.boids[0].pos = np.array((0, 0))
    for idx, boid in enumerate(test_flock.boids[1:]):
        boid.pos = np.array((-10, idx-4))
    sep = test_flock.boids[0].separate(test_flock.boids)
    print(F"sep: {sep}")
        
