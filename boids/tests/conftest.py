from pytest import fixture
from boids import new_boid, Boid
from flock import Flock
from configurable import Config
import random
random.seed('seed')


@fixture
def test_config():
    config = Config()
    config.X = 640
    config.Y = 320

    config.Boid = Config()
    config.Boid.maxforce = 0.03
    config.Boid.maxspeed = 2.0
    config.Boid.target_sep = 250
    config.Boid.neighbordist = 50.0
    
    Boid.clsinit(config)
    return config

@fixture(scope='function')
def screen(test_config):
    return pygame.display.set_mode((test_config.X, test_config.Y))

@fixture(scope='function'):
def boid(test_config):
    return new_boid(test_config.X, test_config.Y)

@fixture(scope='function')
def test_flock(test_config):
    flock = Flock()
    flock.add_boids(new_boid(test_config) for _ in range(10))
    return flock
