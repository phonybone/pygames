* Game update loop
Differentiate between framework space and game space
(sort of like kernel space vs user space)

th.Event: wait() waits until event is set; that is, calling set() 
wakes waiting threads

Have a fw thread that looks like this:

render_start = Event()    # if set then ok to start render
render_start.set()
render_done = Event()     # if set, then render has finished
render_done.clear()

while True:
    time.sleep(h)
    if render_done.is_set():
        display.flip()
	render_start.set()
    else:
        log.debug('frame not ready')    


And the a gs thread that looks like:
while True:
    render_start.wait()
    render_start.clear()

    render_done.clear()           # announce drawing is in progress
    self.render(self.screen)      # do all the drawing stuff
    render_done.set()             # announce that drawing is complete

* Gaming tutorials of interest
Procgen Benchmark: Generating landscapes w/AI
https://github.com/openai/procgen

Dealing with the keyboard and keyup events
https://blog.robertelder.org/detect-keyup-event-linux-terminal/
