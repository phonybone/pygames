'''
Car events (as exceptions)
'''


class CarEvent(Exception):
    def __init__(self, car):
        super().__init__()
        self.car = car


class NextLaneEvent(CarEvent):
    pass


class CarStopped(CarEvent):
    pass


class NoLaneException(CarEvent):
    pass


class EndOfRoad(CarEvent):
    pass
