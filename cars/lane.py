import math

from numerics import path_length

ROADMAP = {}   # key = lane_id, value = Lane()


class Lane:
    def __init__(self, id, speed_at, pos_at):
        """
        Lane()
        speed_at and pat_fn are functions that take a single parameter 't' where 0 <= t <= 1.
        speed_at(t) returns the desired speed of the lane.
        pos_at(t) returns (x,y) coords of the lane position at t.
        """
        if id in ROADMAP:
            raise KeyError(F"Lane {id} already exists")

        self.id = id
        self.speed_at = speed_at
        self.pos_at = pos_at
        self.cars = []          # change to OrderedDict? still doesn't get you car+1...
        self.exits = {}         # list of other lane objects to which vehicles may exit
        self.__length = None
        ROADMAP[self.id] = self

    def __str__(self):
        return F"<Lane id={self.id} len={self.length}>"

    @property
    def length(self):
        if self.__length is None:
            self.__length = path_length(self.pos_at)
        return self.__length

    def pos(self, t):
        return self.pos_at(t)

    def speed(self, x):
        t = self.d2t(x)
        return self.speed_at(t) if callable(self.speed_at) else self.speed_at

    def d2t(self, dist):
        return dist / self.length

    def t2d(self, t):
        return t * self.length

    def enter(self, car):
        self.cars.insert(0, car)

    def add_exit(self, lane):
        self.exits[lane.id] = lane

    def next_lane(self, lane_id):
        return self.exits[lane_id]

    @staticmethod
    def const_speed_lane(speed):
        def csl(t):
            return speed
        return csl


NULL_LANE = Lane('null', lambda x: 0, lambda x: (0, 0))
ROADMAP['null'] = NULL_LANE


if __name__ == '__main__':
    def main():
        lane = Lane('cir0', speed, circle)
        print(f"lane length: {lane.length}")

    def circle(t):
        x = math.cos(t * math.pi)
        y = math.sin(t * math.pi)
        return x, y

    def speed(t):
        return 1

    main()
