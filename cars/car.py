import math

from lane import Lane, NULL_LANE, ROADMAP
import events

MPS2MPH = 3600.0 / 1609.3         # convert m/s -> mph
MPH2MPS = 1609.3 / 3600           # convert mph -> m/s


class Car:
    def __init__(self, max_v, max_accel, max_decel):
        """ All car units in m/s """
        self.max_v = max_v
        self.max_accel = max_accel
        self.max_decel = max_decel
        self.x = 0              # distance down lane
        self.v = 0

        self.lane = NULL_LANE
        self.path = []

    def dashboard(self):
        """ return a string representing the car's state """
        mph = self.v * MPS2MPH
        return f"speed={self.v:.1f}m/s ({mph:.0f} mph) x={self.x:.2f}"

    @property
    def stopping_dist(self):
        return abs(0.5 * self.v * self.v / self.max_decel)

    def move(self, h):
        """
        lane_speed: a function on [0, 1] that returns the max speed allowable for that position
        h: time step
        """
        lane = self.lane
        if self.v > 0:
            x_new = self.x + self.v * h
        else:
            x_new = self.x + self.max_accel * h

        if x_new > lane.length:  # change to 'while'?
            x_new -= self.lane.length
            lane = self.lane_transition()

        A = lane.speed(x_new) - self.v
        A = bracket(self.max_decel * h, A, self.max_accel * h)
        self.v = max(0, self.v + A)
        if self.v == 0:
            raise events.CarStopped(self)
        self.x += self.v

        # don't catch this here; rely on earlier test
        # if self.x > lane.length:
        #     raise events.NextLaneEvent(self)

    def lane_transition(self):
        ''' callback: when self.x > self.lane.length '''
        new_lane = self.get_next_lane()
        print(F"moving from lane {self.lane.id} to lane {new_lane.id}")
        self.x -= self.lane.length
        self.lane = new_lane
        return new_lane

    def get_next_lane(self):
        if len(self.path) > 0:
            lane_id = self.path.pop(0)
        else:
            raise events.EndOfRoad(self)
        return ROADMAP[lane_id]


def bracket(mn, x, mx):
    return min(max(mn, x), mx)


if __name__ == '__main__':
    car = Car(75.0, 5.0, -10.0)
    car.v = 1

    def lane_speed_60(x):
        return 60 * MPH2MPS

    def lane_speed_30(x):
        return 30 * MPH2MPS

    def lane_speed_0_60(t):
        ''' rise 0 -> 60 '''
        return 60 * MPH2MPS * (1 - 1 / (1 + (t * t)))

    def lane_speed_0_60_0(t):
        return 30 * MPH2MPS * (1 - math.cos(2.0 * math.pi * t))

    def line1000(t):
        return t * 1000, 0

    def circle100(t):
        r = 100
        x = r * math.cos(t * math.pi)
        y = r * math.sin(t * math.pi)
        return x, y

    lane = Lane('1-2', Lane.const_speed_lane(60 * MPH2MPS), line1000)
    lane.add_exit(Lane('2-3', Lane.const_speed_lane(30 * MPH2MPS), line1000))
    car.path = ['1-2', '2-3']

    h = 1.0                     # time step in seconds
    H = 0

    try:
        while True:
            print(f"\nH={H:.3f}, x={car.x:.3f}")
            try:
                car.move(h)
            except events.NextLaneEvent as e:
                car.lane_transition()
            print(f"car: {car.dashboard()}")
            H += h
    except events.EndOfRoad as e:
        print(f"reached end of road")
    # except events.CarStopped as e:
    #     print(F"car stopped at {car.x}")
