import math


def path_length(f, t0=0.0, t1=1.0, h=0.001):
    '''
    Compute the length of an arbitrary path defined by f.
    f: function of one variable (t) that returns a tuple (x, y)
    t0, t1: range on which to compute path length (default (0.0, 1.0)).
    h: step size
    '''
    len = 0.0
    x, y = f(t0)
    i = 1
    t = t0 + h
    while t <= t1:
        x0, y0 = f(t)
        dx = x0 - x
        dy = y0 - y
        len += math.sqrt(dx*dx + dy*dy)
        x, y = x0, y0
        t = t0 + i * h
        i += 1
    return len
